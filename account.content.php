<?php



?>

	
	<main class="container personofficedate">
		  
    <div class="row">
        <div class="col-12 account-title">
            ЛИЧНЫЙ КАБИНЕТ
        </div>
    </div>
	<div class="row">
		  <style>
              .form-group
              {
                  margin-bottom: 10px;
              }
          </style>
	    <form class="col-6" method="GET" action="account.action.php">
			<div class="row">
                <div class="col-12 form-group" style="margin-top: 32px ;">
                    <span class="date">Ваши данные</span>
                </div>
                <div class="col-12 form-group">
                    <label for="fio">Контактное лицо(ФИО):</label>
                    <input type="text" class="form-reg-new-person" id="fio" placeholder="Сергей" name="fio" value="<?= $user->userName ?>"/>
                </div>
                <div class="col-12 form-group">
                    <label for="number">Контактный телефон:</label>
                    <input type="text" class="form-reg-new-person"  id="number" name="number" placeholder="+7 916 000-00-00" value="<?= $user->userPhone ?>"/>
                </div>
                <div class="col-12 form-group">
                    <label for="mail">E-mail адрес:</label>
                    <input type="email" class="form-reg-new-person"  id="email" name="email" placeholder="mail@company.ru"value="<?= $user->email ?>"/>
                </div>
			</div>
			<div class="row">	
                <div class="col-12 form-group" style="margin-top: 32px;">
                    <span class="date">Адрес доставки</span>
                </div>
                <div class="col-12 form-group">
                    <label for="town">Город:</label>
                    <input type="text" class="form-reg-new-person" id="town" name="town" placeholder="Москва" value="<?= $user->userTown ?>"/>
                </div>
                <div class="col-12 form-group">
                    <label for="street">Улица:</label>
                    <input type="text" class="form-reg-new-person"  id="street" name="street" placeholder="Название улицы" value="<?= $user->userStreet ?>"/>
                </div>
                <div class="col-6 form-group">
                    <label for="house">Дом:</label>
                    <input type="text" class="form-address"  id="house" name="house" placeholder="10" value="<?= $user->userHouse ?>"/>
                </div>
                <div class="col-6 form-group">
                    <label for="apartment">Квартира:</label>
                    <input type="text" class="form-addressapartment"  id="apartment" name="apartment" placeholder="150" value="<?= $user->userApartment ?>"/>
                </div>
			</div>
			<div class="row">
				<div class="col-12 form-group" style="margin-top: 32px;">
					<span class="date">Изменение пароля</span>
				</div>
				<div class="col-12 form-group">
					<label for="pws">Введите новый пароль:</label>
					<input type="password" class="form-reg-new-person" name="pws" id="pws"/>
				</div>
				<div class="col-12 form-group">
					<label for="pws">Повторите новый пароль:</label>
					<input type="password" class="form-reg-new-person" name="pws2" id="pws2"/>
				</div>
				
				<div class="col-12 form-group">
					<span>
						<?= @$_GET["error"] ?>
					</span>
				</div>
				<div class="col-12 form-group" style="margin-bottom: 16px;">
					<button type="submit" class="save">Сохранить</button>
				</div>
			</div>
		</form>
        <div class="col-6">
			<div class="row">
			    <div class="col-12 form-group" style="margin-top: 32px;">
					<span class="date">Ваши заказы</span>
			    </div>
			 </div>
            <?php
            $orders = Order::getUserOrdersList($user->id);
            foreach ($orders as $order) {

            ?>
		    <div class="row">
				<div class="col-4">
					<span class="orderinfo">№<?= $order->id ?> <br/></span>
					<span class="orderprice">(<?= $nombre_format($order->getLinesSum()); ?> руб.)<br/> </span>
					<span class="orderdatatime"><?= date("d.m.Y в H:i:s", strtotime( $order->confirm_time ) ) ?></span>
				</div>

                <div class="col-8 orderstatuswait">
                    <span style="float: right"><?= $order->status_name_cust ?></span>
                </div>
            </div>
        <?php } ?>
	    </div>
	</main>
	
		