$(document).ready(function()
{
	$('.product-delete').on('click', function()
	{
		
		//Проверить, что имя не пустое и поорать
		if (!confirm("Вы уверены, что хотите удалить?"))
		{
			return;
		}
		var cid = $(this).attr("category_id");
		$.post(
			"/shop/Action/category.delete.php",
			{
				CATEGORY_ID : cid
			},
			function(data)
			{
				alert(data);
				location.reload();
			},
			"text"
			)
			.fail(function(data)
			{
				alert("Не получилось :(\r\n" + data);
			});
		
	});
	
});

$(document).ready(function()
{
	$('.add-new-category').on('click', function()
	{
		var $input = $('.adding-new-cat');
		//Проверить, что имя не пустое и поорать
		if ($input.val() == "")
		{
			alert("Вы не добавили");
			return;
		}
		$.post(
			"/shop/Action/category.create.php",
			{
				CATEGORY_ID : $input.attr("category_id"),
				CATEGORY_NAME : $input.val()
			},
			function(data)
			{
				//alert("Ура, мы смогли!!!111");
				$('<div class="alert alert-success alert-dismissible" role="alert">Успешно добавлено<button type="button" class="close" data-dismiss="alert" aria-label="Close">  <span aria-hidden="true">&times;</span></button></div>')
					
					.appendTo($('.add-new-category').parent())
					.alert();
					location.reload();
			},
			"text"
			)
			.fail(function()
			{
				alert("Не получилось :(");
			});
		
	});
});