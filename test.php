<html>
    
    <head>
        <title>Просмотр логов</title>
		<script>
		    window.createTableSorter =  window.createTableSorter || function(tableWrapperClass)
            {

    var getCellValue = function(tr, idx){ return tr.children[idx].innerText || tr.children[idx].textContent; }

    var comparer = function(idx, asc) { return function(a, b) { return function(v1, v2) {
            return v1 !== '' && v2 !== '' && !isNaN(v1) && !isNaN(v2) ? v1 - v2 : v1.toString().localeCompare(v2);
        }(getCellValue(asc ? a : b, idx), getCellValue(asc ? b : a, idx));
    }};

    // do the work...
    Array.prototype.slice.call(document.querySelectorAll('.' + tableWrapperClass +' th')).forEach(function(th) { th.addEventListener('click', function() {
            var table = th.parentNode
            while(table.tagName.toUpperCase() != 'TABLE') table = table.parentNode;
            Array.prototype.slice.call(table.querySelectorAll('tr:nth-child(n+2)'))
                .sort(comparer(Array.prototype.slice.call(th.parentNode.children).indexOf(th), this.asc = !this.asc))
                .forEach(function(tr) { table.appendChild(tr) });
        })
    });
};
		</script>
    </head>
    <body>

        <style>
            .action_data
            {
                width: 150px;
                text-align:center;
            }
            .user_name
            {
                width: 100px;
                text-align:center;
            }
            .type
            {
                width: 180px;
                text-align:center;
            }
            .result
            {
                width: 100px;
                text-align:center;
            }
            .comment
            {
                
            }
            tr:not(.header):hover
            {
                background-color:#e5e5e5;
            }
            td
            {
                padding: 5px 2px;
                border-bottom: 1px solid #e2e2e2;
            }
            th
            {
               cursor:pointer;     
            }
            
        </style>




<?php
  
  include_once("./Model/common.php");

	include_once(SITE_ROOT."Model/Log.php");
	
	
	$list = Log::getLogList();
	
	?>
	
	    <table cellspacing="0" border="0" width="100%" class="table">
	        
	        <tr class="header">
	            <th class="action_data">Дата</th>
	            <th class="user_name">Юзер</th>
	            <th class="type">Тип</th>
	            <th class="result">Результат</th>
	            <th class="comment">Комментарий</th>
	        </tr>
	        
	
        	<?php foreach ($list as $rec) { ?>
	    
    	    <tr>
    	        <td class="action_data"><?= $rec["action_data"];?></td>
    	        <td class="user_name"><?= $rec["user_name"];?></td>
    	        <td class="type"><?= $rec["type"];?></td>
    	        <td class="result"><?= $rec["result"];?></td>
    	        <td class="comment"><?= $rec["comment"];?></td>
    	    </tr>
	    
    	    <?php } ?>
    	    
    	    
        </table>
        
        <script>
            createTableSorter('table');
            
        </script>

    </body>
    
</html>