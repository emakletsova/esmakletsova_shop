<?php
    $order = new Order($ORDER_ID);
    $user = new User($order->user_id);
?>

<style>
    .status-select-option-1
    {
        color: #0a8eaf;
    }
    .status-select-option-2
    {
        color:  #1ba254;
    }
    .status-select-option-3
    {
        color: #ad5a00;
    }
    .status-select-option-4
    {
        color: #a01ba2;
    }
    .status-select-option-5
    {
        color: #6c6c6c;
    }

</style>
			<div class="col-9 order-list admin-panel">
				<div class="row">
					<div class="col-12 order-list-title">
						ЗАКАЗ
                        <span style="font-family: Proxima Nova Light; font-size: 48px; color: #3498db;">№<?= $order->id; ?></span>
                        <span class="statusthisorder">
                            <span class="bracket">(</span>
    
                            <select class="order-this-status-select order-status-select" oid="<?= $order->id ?>"
                                    style="font-family: Proxima Nova Light; font-size: 24px;
                                    margin:0 -6px;
                                    border-bottom: 2px dotted #ad5a00">
    
                                <?php
                                $statusList = (new OrderStatus())->getElements();
                                foreach ($statusList as $status) {
                                    ?>
    
                                    <option
    
                                            class="status-select-option-<?= $status->id ?>"
                                            w="<?php
                                            
                                                switch ($status->id )
                                                {
                                                  case 2:
                                                    { echo 80; break; }
                                                  case 3:
                                                    { echo 105; break; }
                                                  case 4:
                                                    { echo 110; break; }
                                                  case 5:
                                                    { echo 120; break; }
                                                  case 6:
                                                    { echo 100; break; }
                                                }
                                            
                                            ?>"
                                        <?= $order->status == $status->id ? "selected" : "" ?>
                                             value="<?= $status->id ?>">
                                   
                                    <span style="text-decoration-style: dashed;"><?= $status->name ?></span>
                                    </option>
                                  <?php
    
                                }
                                ?>
    
    
                            </select>
                            <span class="bracket">)</span>
                        </span>
					</div>
				</div>
				<div class="row order-list-head linecart">
					<div class="col-12 order-list-head-item">
							СОДЕРЖИМОЕ ЗАКАЗА
					</div>
					
				</div>
                <?php foreach ($order->lines as $line) {
                    ?>
				<div class="row order-list-item-details"
                     pid="<?= $line->product->id; ?>"
                     ptype="<?= $line->product_type; ?>">
					<div class="col-3 order-name">
						<a href="/shop/item_information_admin.php?PRODUCT_ID=<?=$line->product->id;?>">
							<?= $line->product->name; ?>
						</a>
					</div>
					<div class="col-2 order-price-one">
						<span><?= $line->product->getPrintPrice(); ?></span>
					</div>
					<div class="col-2 order-amount-trade">
							<input class="product-count" style="width: 50px;text-align: right;" type="number" min="1" max="99" value="<?= $line->quantity; ?>" />
					</div>
					<div class="col-2 order-price-after">
							<?= $nombre_format($line->countSum()); ?> руб.
					</div>
					<div class="col-3 order-delete">
						<span class="order-delete-clicker" style="border-bottom: 1px dotted #c73b3b; cursor: pointer;">
                            убрать из заказа
                        </span>
					</div>
				</div>
                <?php } ?>
				<div class="row order-list-total-amount">
					<div class="col-12" style="text-align: right; white-space: nowrap;">
                        <span class="amount-price-text"
                              style="padding-top: 14px; line-height: 16px; display: inline-block;">
                                ИТОГОВАЯ<br/> СУММА
                        </span>&nbsp;
                        <span class="amount-price">
                          <?= $nombre_format($order->getLinesSum()); ?><span style="font-family: Proxima Nova Light; font-size: 22px; ">руб.</span>
                        </span>
					</div>
				</div>
				<div class="row empty-line">
				</div>
				<div class="row order-list-head linecart">
					<div class="col-12 order-list-head-item">
							ИНФОРМАЦИЯ О ЗАКАЗЕ
					</div>
				</div>	
				<div class="row order-list-information">
					<div class="col-4">
						<div class="row">
							<div class="col-12">
							    <span class="title-information">Контактное лицо(ФИО):</span>
								<input class="description FIO" type="text" value="<?= $user->userName; ?>"/>
							</div>					
							<div class="col-12">
								<span class="title-information">Контактный телефон:</span>
								<input class="description phone" type="text" value="<?= $user->userPhone; ?>"/>
							</div>									
							<div class="col-12">
								<span class="title-information">E-mail:</span>
								<input class="description email" type="text" value="<?= $user->email; ?>"/>
							</div>	
						</div>	
					</div>
					<div class="col-4">
						<div class="row">
							<div class="col-12">
								<span class="title-information">Город:</span>
								<input class="description your-town" type="text" value="<?= $order->town; ?>"/>
							</div>
							<div class="col-12">
								<span class="title-information">Улица:</span>
								<input class="description your-street" type="text" value="<?= $order->street; ?>"/>
							</div>
						</div>
							
						<div class="row">
							<div class="col-6">
								<span class="title-information">Дом:</span>
								<input class="description form-address" type="text" value="<?= $order->house; ?>"/>
							</div>
							<div class="col-6">
								<span class="title-information">Квартира:</span>
								<input class="description form-addressapartment" type="text" value="<?= $order->apartment; ?>"/>
							</div>
						</div>
					</div>
					<div class="col-4">						
						<span class="title-information">Способ доставки:<br/></span>
						<span class="description-delivery"><?= $order->type_delivery_name ?></span>
					</div>									
					<div class="col-8">							
						<span class="title-information">Комментарий к заказу:<br/></span>
						<textarea class="description comment" style="resize: none; width:100%;" rows="3"  placeholder="Комментарий"><?= $order->text_comment ?></textarea>
					</div>
				</div>
                <div class="row">
                    <div class="col-6 offset-6">
						<span class="delete-product cancel-order" style="cursor:pointer; float: right;" >
							Отменить заказ
						</span>
                    </div>
                </div>
            </div>


<script>
    $(document).ready(function () {
        $('.order-delete-clicker').on('click', function () {

            if (!confirm( "Удалить?" ))
                return false;
            var param = {};
            param["order_id"] = '<?= $order->id ?>';
            param["product_id"] = $(this).parents('.order-list-item-details').attr('pid');
            param["product_type"] = $(this).parents('.order-list-item-details').attr('ptype');

            $.post(
                "/shop/Action/product.removeFromCart.php",
                param,
                function (data) {
                    console.log(data);
                    location.reload();
                },
                "text"
            )
                .fail(function (data) {
                    alert("Не получилось :(\r\n" + data);
                });

        });

        $('.product-count').on('change',function(){

            var param = {};
            param["product_id"] = $(this).parents('.order-list-item-details').attr('pid');
            param["order_id"] = '<?= $order->id ?>';
            param["product_type"] = $(this).parents('.order-list-item-details').attr('ptype');
            param["quantity"] = $(this).val();
            $.post(
                "/shop/Action/product.changeAmountCart.php",
                param,
                function (data) {
                    console.log(data);
                    location.reload();
                },
                "text"
            )
                .fail(function (data) {
                    alert("Не получилось :(\r\n" + data);
                });

        });

        $('.cancel-order').on('click', function () {

            if (!confirm( "Отменить?" ))
                return false;
            var param = {};
            param["ORDER_ID"] = '<?= $order->id ?>';
            $.post(
                "/shop/Action/order.cancel.php",
                param,
                function (data) {
                    console.log(data);
                    location.reload();
                },
                "text"
            )
                .fail(function (data) {
                    alert("Не получилось :(\r\n" + data);
                });

        });

    });

</script>
<script>
    $('.order-list-information input, .order-list-information textarea').on('change', function()
    {

        var param = {};

        param["ORDER_ID"] = '<?= $order->id; ?>';

        param["town"] = $('.your-town').val();
        param["street"] = $('.your-street').val();
        param["house"] = $('.form-address').val();
        param["apartment"] = $('.form-addressapartment').val();
        param["text_comment"] = $('.comment').val();

        param["type_delivery"] = '<?= $order->type_delivery ?>';
        
        param["saveUser"] = true;

        param["email"] = $('.email').val();
        param["phone"] = $('.phone').val();
        param["fio"] = $('.FIO').val();


        $.post(
            "/shop/Action/order.step_2.php",
            param,
            function(data)
            {
                console.log(data);
                location.reload;
            },
            "text"
        )
            .fail(function(data)
            {
                alert("Не получилось :(\r\n" + data);
            });
    });



</script>


<script>

    //order-status-select
    $('.order-status-select').on('change', function (e) {

        var $status = $(this).children("option:selected");
        if (!confirm( "Сменить статус на " + $status.text().trim() + "?"  )) {
            return false;
        }
        $(this).css("color", $status.css("color"))
            .css("width", $status.attr("w"))
            .css("border-color", $status.css("color"));
        $('.bracket').css("color", $status.css("color"));
        var param = {};
        param["ORDER_ID"] = $(this).attr("oid");
        param["STATUS_ID"] = $status.val();
        $.post(
            "/shop/Action/order.setStatus.php",
            param,
            function (data) {
                console.log(data);
                // location.reload();
            },
            "text"
        )
            .fail(function (data) {
                alert("Не получилось :(\r\n" + data);
            });

    })
        .each(function () {
            $(this).css("color", $(this).children("option:selected").css("color"))
                .css("width", $(this).children("option:selected").attr("w"))
                .css("border-color", $(this).children("option:selected").css("color"));
        });
    $('.bracket').css("color",  $('.order-status-select').children("option:selected").css("color"));


</script>
