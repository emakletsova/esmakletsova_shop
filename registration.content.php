﻿	
	<main class="container registrationperson">
		<div class="row">
			<div class="col-12 order-title">
				РЕГИСТРАЦИЯ
			</div>
		</div>
	


		<form class="row" method="GET" action="registration.action.php">
		  
		   <div class="col-6">
			    <div class="col-12 form-group" style="margin-top:30px;">
					<label for="fio">Контактное лицо(ФИО):</label>
					<input type="text" class="form-reg-new-person" id="fio" name="fio" placeholder=""/>
			    </div>
			    <div class="col-12 form-group">
					<label for="mail">E-mail адрес:</label>
					<input type="email" class="form-reg-new-person"  id="email" name="email" placeholder="mail@company.ru"/>
			    </div>
				<div class="col-12 form-group">
					<button type="submit" class="regnewperson" onclick="return validate();">Зарегистрироваться</button>
				</div>
		  </div>
		  <div class="col-6">
			    <div class="col-12 form-group" style="margin-top:30px;">
					<label for="pws">Пароль:</label>
					<input type="password" class="form-reg-new-person"  id="pws" name="pws" value="" onkeyup='checkPasswordMatch();' style="font-family: Supermolot Bold; font-size:24px;"/>
			    </div>
				<div class="col-12 form-group">
					<label for="pws2">Повторите пароль:</label>
					<input type="password" class="form-reg-new-person"  id="pws2" name="pws2" value="" onkeyup='checkPasswordMatch();' style="font-family: Supermolot Bold; font-size:24px;"/>
			    </div>
				<div class="registrationFormAlert" id="divCheckPasswordMatch">
				</div>
		  </div>
		</form>

        <?= @$_GET["error"] ?>
	</main>
	

	<script>
		
			function validateEmail(email) {
				var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
				return re.test(String(email).toLowerCase());
			}		
			
			function validate()
			{					
				var email = document.getElementById('email');
				if (!validateEmail(email.value))
				{
					email.focus();
					alert('Проверьте email!');
					return false;
				} 
				
				if(!checkPasswordMatch())
				    return false;
				
				return confirm('Сохранить?');
			}
			
		    function checkPasswordMatch() 
			{
				var divCheckPasswordMatch = document.getElementById('divCheckPasswordMatch');
				
				if (document.getElementById('pws').value ==
						document.getElementById('pws2').value) 
				{
					if (document.getElementById('pws').value == "")
					{
						document.getElementById('pws').focus();
						alert('Введите пароль!');
						divCheckPasswordMatch.innerHTML = "";
						return false;
					} else{
						divCheckPasswordMatch.style.color = 'green';
						divCheckPasswordMatch.innerHTML = 'Пароли совпадают';
					}
					return true;
				} else 
				{
					divCheckPasswordMatch.style.color = 'red';
					divCheckPasswordMatch.innerHTML = 'Пароли не совпадают';
					return false;
				}
			}
			
			
			
			
			
		</script>
