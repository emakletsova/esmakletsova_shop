﻿	
	<main class="container registrationperson">
	
		<div class="row">
			<div class="col-12 login-title">
				Восстановление пароля
			</div>
		</div> 		 
			
		<div class="row">
		  
			<div class="offset-3 col-6">
				<form action="recover.action.php" method="GET">
					<div class="col-12 form-group" style="margin-top: 32px;">
						<span class="regperson">Восстановлление пароля</span>
					</div>
					<div class="col-12 form-group">
						<label for="mail">E-mail адрес:</label>
						<input type="email" class="form-reg-new-person"  id="email" name="email" placeholder="mail@company.ru"/>
					</div>
					<div class="col-12 form-group">
						<button type="submit" class="enterperson" style="width: auto;">Восстановить</button>
					</div>
				</form>
			</div>
		</div>
	
	</main>
	
	<script>
		
		function validateEmail(email) {
			var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
			return re.test(String(email).toLowerCase());
		}		
		
		function validate()
		{					
			var email = document.getElementById('email');
			if (!validateEmail(email.value))
			{
				email.focus();
				alert('Проверьте email!');
				return false;
			} 
			if (!confirm('Отправить?'))
				return false;

				
			return true;
		}
	</script>