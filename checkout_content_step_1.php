<?php

?>


<main class="container shoppingcart">
    <div class="row">
        <div class="col-12 cart-title">
            Оформление заказа
        </div>
    </div>

    <div class="row">
        <div class="col-12 contact-information">
            <span style="color:#ed1651;">1.</span>
            Контактная информация
        </div>
    </div>
    <div class="row information-click">

        <div class="col-5">
            <form  action="registration.action.php" method="GET" >
                <div class="col-12 form-group" style="margin-top: 33px;">
                    <span class="forneworder">Для новых покупателей</span>
                </div>


                <div class="col-12 form-group">
                    <label for="fio">Контактное лицо(ФИО):</label>
                    <input type="text" class="form-new-person" id="fio" name="fio" placeholder="Сергей Сергеевич"/>
                </div>


                <div class="col-12 form-group">
                    <label for="number">Контактный телефон:</label>
                    <input type="text" class="form-new-person"  id="number" name="number"/>
                </div>


                <div class="col-12 form-group">
                    <label for="email">E-mail:</label>
                    <input type="email" class="form-new-person"  id="email" name="email"/>
                </div>
                <div class="col-12 form-group">
                    <input type="hidden" id="return_url" name="return_url" value="checkout.php?STEP=2"/>
                    <button type="submit" class="continue">Продолжить</button>
                </div>
            </form>
        </div>
        <div class="col-7">
            <form  action="login.action.php" method="GET" >
                <div class="col-12 form-group" style="margin-top: 33px;">
                    <span class="quickenter">Быстрый вход</span>
                </div>


                <div class="col-12 form-group">
                    <label for="email">Ваш e-mail:</label>
                    <input type="email" class="form-reg-person" id="email" name="email" placeholder="mail@company.ru"/>
                </div>


                <div class="col-12 form-group">
                    <label for="pws">Пароль:</label>
                    <input type="password" class="form-reg-person"  id="pws" name="pws"/>
                </div>

                <div class="col-12 form-group">
                    <button type="submit" class="enterin">Войти</button>
                    <span style="padding-left: 20px;">
							<a class="restorepws" href="recover.php">Восстановить пароль</a>
						</span>
                </div>

                <input type="hidden" id="return_url" name="return_url" value="checkout.php?STEP=2"/>
            </form>
        </div>
    </div>
    <div class="row" style="margin-top: 30px;border-top: 2px solid #f6f6f6;">
        <div class="col-12 contact-info">
            <span style="color:#ed1651;">2.</span>
            Информация о доставке
        </div>

    </div>
    
    <div class="row" style="border-top: 2px solid #f6f6f6;">
        <div class="col-12 contact-info">
            <span style="color:#ed1651;">3.</span>
            Подтверждение заказа
        </div>

    </div>
</main>
