<?php

	
?>
	
			<div class="col-9 order-list admin-panel">
				<div class="row">
					<div class="col-12 order-list-title">
						ПРОСМОТР ТОВАРА
					</div>
				</div>
				<div class="row order-list-head linecart">
					<div class="col-12 order-list-head-item">
							ИНФОРМАЦИЯ О ТОВАРЕ
					</div>
					
				</div>			
				<div class="row product-list-information">
					<div class="col-6">
						<div class="row">
							<div class="col-12" style="margin-top: 20px;">						
								<span class="title-information-product">Название товара:<br/></span>
								<input class="product-name-admin" type="text" value="<?= $product->name; ?>"/>
							</div>									
							<div class="col-12">							
								<span class="title-information-product">Описание товара:<br/></span>
								<textarea class="description product-description" style="resize: none; width:100%;" rows="3"> <?= $product->description; ?></textarea>
							</div>
						</div>
					</div>
					<div class="offset-1 col-5">
						<div class="row">
							<div class="col-12" style="margin-top: 20px;">						
								<span class="title-information-product">Бейджик:<br/></span>
							</div>
							<div class="col-12">						
								<input name="badge" type="radio" value="miss" id="missing" 
									<?= !($product->newProduct || $product->popularProduct || $product->saleProduct ) ? 'checked="checked"' : "" ?>
								 />
								<label for="missing"  style="color:black;">Отсутствует</label>
							</div>	
							<div class="col-12">						
								<input name="badge" type="radio" value="new" id="newproduct"
									<?= $product->newProduct ? 'checked="checked"' : "" ?> />
								<label for="newproduct" style="color:black;">NEW</label>
							</div>	
							<div class="col-12">						
								<input name="badge" type="radio" value="hot" id="hotproduct"
									<?= $product->popularProduct ? 'checked="checked"' : "" ?> />
								<label for="hotproduct" style="color:black;">HOT</label>
							</div>
							<div class="col-12">						
								<input name="badge" type="radio" value="sale" id="saleproduct"
								<?= $product->saleProduct ? 'checked="checked"' : "" ?> />
								<label for="saleproduct" style="color:black;">SALE</label>
							</div>								
						</div>
					</div>
				</div>	
				<div class="row empty-line">
				</div>
				<div class="row order-list-head linecart">
					<div class="col-12 order-list-head-item">
							ФОТОГРАФИИ ТОВАРА
					</div>
				</div>	
				
				<div class="row product-list-information" style="padding-top: 8px;">
				<?php 
					foreach ($product->getMedias(false) as $media)
					{
				?>
					<div class="col-3 product-item">
						<div class="row">
							<div class="col-12 img-product-kat">
								<img src="<?= $media->path; ?>" class="product-image" mid='<?= $media->id; ?>' />
							</div>
							<div class="col-12 ">
								<span class="change-img">Изменить</span>
							</div>
							<div class="col-12">
								<span class="delete-img">Удалить</span>
							</div>
						</div>			
					</div>
					<?php } ?>

                    <!-- сюда вставляем новые -->
					<div class="col-3 product-item ">
						<div class="row">
							<div class="col-12 img-product-kat nophoto">
								<span class="nofile">не загружено</span>
							</div>
							<div class="col-12">
								<span class="add-img">Загрузить</span>
							</div>
						</div>				
					</div>
				</div>
				
				<div class="row empty-line">
				</div>
			    <div class="row order-list-head linecart">
					<div class="col-12 order-list-head-item">
							ВАРИАЦИИ ТОВАРА
					</div>
				</div>
				<div class="row linecart" style="padding-top: 20px;">				
					<div class="col-12">
					<?php 
					
						foreach ($product->types as $type_name)
						{
					?> 		
						<div class="row product-list-information">
							<div class="col-6">						
								<input class="description product-type-description" type="text" value="<?= $type_name; ?>" />
							</div>
							<div class="col-6">						
								<span class="delete-var" style="cursor:pointer;" product_id="<?= $product->id; ?>" >Удалить</span>
							</div>
						</div>
					<?php } ?>											
					</div>								
				</div>
				<div class="row">
					<div class="col-3">
						<span class="delete-product" style="cursor:pointer;" product_id="<?= $product->id; ?>" category_id="<?= $product->category_id; ?>" >
							Удалить товар
						</span>
					</div>
					<div class="offset-5 col-4" style="text-align: right;">
						<span class="save-change save-product" style="cursor:pointer;" product_id="<?= $product->id; ?>"  category_id="<?= $product->category_id; ?>">
							Сохранить изменения
						</span>
					</div>
				</div>
			</div>	
			<script>
                $('.delete-var').on('click',function () {
                    $(this).parents('.product-list-information').find('.product-type-description').val('');
                });
                
				<?php include("item_information_admin.script.media.js"); ?>
				<?php include("item_information_admin.script.js"); ?>
			</script>			