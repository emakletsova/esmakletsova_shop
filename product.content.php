<?php
  //var_dump($product);
?>
<link rel="stylesheet" href="https://unpkg.com/swiper/css/swiper.css">
<link rel="stylesheet" href="https://unpkg.com/swiper/css/swiper.min.css">


<script src="https://unpkg.com/swiper/js/swiper.js"></script>
<script src="https://unpkg.com/swiper/js/swiper.min.js"></script>


<main class="container aboutproduct">
    <div class="row">
        <div class="col-12 category-title">
          <?= $category->name; ?>
        </div>
        <div class="col-12" style="background-color: #e4e0d6; padding: 0;">
            <a class="backcatalogy" href="category.php?CATEGORY_ID=<?= $category->id; ?>&PAGE=0">ВЕРНУТЬСЯ В КАТАЛОГ</a>
        </div>
    </div>
    <div class="row central-content">
        <div style="height:20px;">
        </div>
    </div>
    <div class="row">
        <div class="col-6 product-gallery" style="">
            <div class="row swiper-container gallery-top" style="width: 500px;">
                <div class="col-8 w-100 img-responsive swiper-wrapper">
                  <?php
                    foreach ($product->getMedias() as $media) {
                      echo "<div class='swiper-slide' style='height: 470px;width: 470px;'>
                                <span style='display: inline-block; height: 100%; vertical-align: middle;'></span>
                                <a  data-fancybox='product' href='$media->path'>
                                    <img src='img/Zoom.png' style='top: 25px; left: 0;' class='product-flag' />
								    <img src='$media->path' class='image-product' />
								</a>
							</div>";
                    }

                  ?>
                </div>
            </div>
            <div class="swiper-button-prev swiper-button-prev-1"></div>
            <div class="row swiper-container gallery-thumbs"
                 style="height: 100px; margin-left: 81px;">

                <div class="swiper-wrapper">
                  <?php

                    foreach ($product->getMedias() as $media) {
                      echo "<div class='swiper-slide'>
								<img src='$media->path' class='slider-preview' />
								
							</div>";
                    }

                  ?>

                </div>


            </div>
            <div class="swiper-button-next swiper-button-next-1"></div>
        </div>
        <div class="col-3">
            <div class="row" style="margin-top: 35px;">
                <div class="col-12" style="font-family: Supermolot Light Italic; font-size:36px; ">
                  <?= $product->name; ?>
                    <hr class="line-horizont"/>
                </div>
                <div class="col-12" style="font-family: Supermolot Light; font-size:14px; ">
                  <?= $product->description; ?>
                </div>
                <div class="col-12" style="<?= $product->types == null ? 'display:none;' : '' ?>">Выберите вариант:
                    <select class="product-type" style="background-color: #e9e9e9;">
                      <?php
                        $i = 0;
                        if ($product->types != null)
                          foreach ($product->types as $type_name) {
                            if ($i == 0) {

                              echo "";
                            }
                            $i++;
                            echo "<option value='$type_name'>$type_name</option>";
                            //<label for='type_ $i'>$type_name</label><br/>";
                          }
                      ?>
                    </select>
                </div>
            </div>
        </div>
        <div class="col-3">
            <div class="row" style="border:2px solid #dbdbdb; width:221px; height:184px; margin-top: 40px;">

                <div class="offset-3 col-9 product-oldprice">
                 <span style="text-decoration:line-through; <?= $product->saleProduct == 0 ? 'display:none;' : '' ?>">
                   <?= $product->getPrintPriceFull() ?> руб.
                     </span>
                </div>


                <div class="offset-2 col-10 product-newprice">
                  <?= $product->getPrintPrice() ?> руб.
                </div>


                <div class="offset-3 col-9 available">
                  <?= $product->inStock == 1 ? "<img src='img/galka.png'/> есть в наличии" : "нет на складе"; ?>
                </div>

                <hr/>

                <div class="row">
                    <div class="offset-4 col-8">
                        <img src="img/buy_button.png"
                             class="buy_button <?= $product->inStock != 1 ? 'disabled' : '' ?>"/>
                    </div>
                </div>
            </div>
            <div class="row">
                <div style="height:20px;">
                </div>
            </div>

            <div class="row">
                <div class="col-2" style="padding:0;">
                    <img src="img/Shipping.png"/>
                </div>
                <div class="col-10 product-advantage">
                    <span style="font-family:Supermolot Bold; font-size: 14px;">БЕСПЛАТНАЯ ДОСТАВКА</span><br/>
                    <span tyle="font-family:Supermolot Light; font-size: 14px;">по всей России</span>
                </div>
            </div>
            <br/>
            <div class="row">
                <div class="col-2" style="padding:0;">
                    <img src="img/Support.png"/>
                </div>
                <div class="col-10 product-advantage">
                    <span style="font-family:Supermolot Bold; font-size: 14px;">ГОРЯЧАЯ ЛИНИЯ</span><br/>
                    <span tyle="font-family:Supermolot Light; font-size: 14px;">8-800-000-00-00 </span>
                </div>
            </div>
            <br/>
            <div class="row">
                <div class="col-2" style="padding:0;">
                    <img src="img/Gifts.png"/>
                </div>
                <div class="col-10 product-advantage">
                    <span style="font-family:Supermolot Bold; font-size: 14px;">ПОДАРКИ</span><br/>
                    <span tyle="font-family:Supermolot Light; font-size: 14px;">каждому покупателю</span>
                </div>
            </div>

        </div>
</main>
<div class="row central-content">
    <div style="height:20px;">
    </div>
</div>

<div class="container">
    <div class="row othertrade">
        <div class="col-11 otherproduct">

            Другие товары из категории «<?= $category->name; ?>»

        </div>
        <div class="col-1">
            <span class="swiper-button-prev-3"></span> <span class="swiper-button-next-3"></span>
        </div>
    </div>
    <div class="row">
        <div class="swiper-container swiper-wrapper-3" style="height: 350px;">
            <div class="swiper-wrapper">
          <?php
    
            $prods = $category->getProductListExceptCurrent($product->id);
            foreach ($prods as $p) {
              ?>
    
                <a class="col-3 comebacktoproduct swiper-slide" href='/shop/product.php?PRODUCT_ID=<?= $p->id; ?>' ;>
                    <div class="row" style="background:#fff;">
                        <div class="col-12 product-image-box slider-preview">
    
                            <img src="<?= $p->medias[0]->path; ?>" style="height:236px;"/>
                            <?= $p->getFlag(); ?>
                        </div>
                        <div class="col-6 product-name">
                            <span style="font-size: 14px;">
                                <?= $p->name; ?>
                            </span>
                        </div>
                        <div class="col-6 product-price">
                            <span style="color:red;">
                                <?= $p->getPrintPrice() ?> руб.
                            </span>
                        </div>
                    </div>
                </a>
    
              <?php
            }
          ?>
            </div>

        </div>
    </div>
</div>

<script>
    $(document).ready(function () {
        $('.buy_button').on('click', function () {

            var param = {};
            param["product_id"] = '<?= $product->id ?>';
            param["product_type"] = $('.product-type option:selected').val();
            $.post(
                "/shop/Action/product.addToCart.php",
                param,
                function (data) {
                    console.log(data);
                    location.reload();
                },
                "text"
            )
                .fail(function (data) {
                    alert("Не получилось :(\r\n" + data);
                });

        });
    });
</script>

<script>

    <!-- Initialize Swiper -->


    var galleryThumbs = new Swiper('.gallery-thumbs', {
        spaceBetween: 15,
        slidesPerView: 4,
        watchSlidesVisibility: true,
        watchSlidesProgress: true,
    });
    var galleryTop = new Swiper('.gallery-top', {
        spaceBetween: 10,
        loop: true,
        loopedSlides: 5,
        navigation: {
            nextEl: '.swiper-button-next-1',
            prevEl: '.swiper-button-prev-1',
        },
        thumbs: {
            swiper: galleryThumbs,
        },
    });

    var qqq = new Swiper('.swiper-wrapper-3', {
        spaceBetween: 0,
        loop: true,
        loopedSlides: 4,
        slidesPerView: 4,
        navigation: {
            nextEl: '.swiper-button-next-3',
            prevEl: '.swiper-button-prev-3',
        }
    });

</script>

