<?php 
	SESSION_START();
	include_once("./Model/common.php");
	
	include_once(SITE_ROOT."Model/product.php");
	include_once(SITE_ROOT."Model/category.php");
	include_once(SITE_ROOT."Model/user.php");
	User::checkSellMan();
	User::checkSuperAdmin();
	
	
	$CATEGORY_ID = isset($_GET["CATEGORY_ID"]) ? $_GET["CATEGORY_ID"] : "";

	$category = $CATEGORY_ID=="" ? null : new Category($CATEGORY_ID);
	
	
	
?>
<?php

	$page_title = "Товары в категории";
	
	
	include("admin.part.head.php");
	include("admin.part.leftmenu.php");
	
	include("items_in_category_admin.content.php");
	
	include("admin.part.footer.php");
	
?>