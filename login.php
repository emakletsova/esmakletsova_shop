<?php
	SESSION_START();
  
  include_once("./Model/common.php");
  
  include_once(SITE_ROOT."Model/user.php");


    if (User::IsCurrUserAuth())
    {
        
        // если авторизован - го в личный кабинет
		header("location:account.php");
		exit();
    }

?> 
<?php

	$page_title = "Вход";
	include("head.php");
	
	
	include("menu.php");
	
	
	
	include("login.content.php");
	
	
	include("footer.php");
?>