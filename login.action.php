<?php
	SESSION_START();
  
  
  include_once("./Model/common.php");
  
  include_once(SITE_ROOT."Model/order.php");
include_once(SITE_ROOT."Model/user.php");
include_once(SITE_ROOT."Model/Log.php");

	$email = $_GET["email"];
  $pws= $_GET["pws"];
  $return_url= @$_GET["return_url"];

	$user = (new User())->loadByEmail($email);

	$oldCart = Order::GetCurrUserCart();
	if ($user == null)
	{
	    Log::insert(2,0,1,"Пользователь $email не найден");
		header("location:login.php?error=".urlencode("Пользователь не найден!"));
		exit();
	}
	if ($user->userPwd != $pws)
	{
        Log::insert(2,0,1,"Пользователь $email не найден: пароли не совпали");
        header("location:login.php?error=".urlencode("Пользователь не найден!"));
		exit();
	}
Log::insert(2,$user->id,0,"Пользователь $email авторизован");

	User::SetCurrUserAuth($user->id);
	//echo $user->id;
  Order::GetCurrUserCart()->mergeCart($oldCart);
  if ($return_url == "") {
    header("location:account.php");
  }
	  else
  {
    header("location:" . $return_url);
  }
  exit();
	
	

?>