<?php
	SESSION_START();
  
  include_once("./Model/common.php");
  include_once(SITE_ROOT."Model/user.php");

  User::UnsetCurrUserAuth();
  session_destroy();
	header("location:login.php");
	exit();
	
?>