<?php

?>


<main class="container shoppingcart">
    <div class="row central-content">
            <div style="height:20px;">
            </div>
    </div>
    <div class="row">
        <div class="col-12 contact-info">
            <span style="color:#ed1651;">1.</span>
            Контактная информация
        </div>
    </div>

    <div class="row">
        <div class="col-12 information-delivery">
            <span style="color:#ed1651;">2.</span>
            Информация о доставке
        </div>
    </div>
    <div class="row information-click">

        <div class="col-5">
            <div class="row">
                <div class="col-12 form-group" style="margin-top: 32px;">
                    <span class="deliveryadress">Адрес доставки</span>
                </div>
                <div class="col-12 form-group">
                    <label for="town">Город:</label>
                    <input type="text" class="form-reg-new-person your-town" id="town" name="town"
                           placeholder="Ваш город" value="<?= $cart->town ?>"/>
                </div>
                <div class="col-12 form-group">
                    <label for="street">Улица:</label>
                    <input type="text" class="form-reg-new-person your-street"  id="street" name="street"  value="<?= $cart->street ?>"/>
                </div>
                <div class="col-6 form-group">
                    <label for="house">Дом:</label>
                    <input type="text" class="form-address"  id="house" name="house"  value="<?= $cart->house ?>"/>
                </div>
                <div class="col-6 form-group">
                    <label for="apartment">Квартира:</label>
                    <input type="text" class="form-addressapartment"  id="apartment" name="apartment"  value="<?= $cart->apartment ?>"/>
                </div>


            </div>
        </div>
        <div class="col-3">
            <div class="row">
                <div class="col-12 form-group" style="margin-top: 33px;">
                    <span class="choosedelivery">Способ доставки</span>
                </div>

                <?php
                    $dt = new DeliveryType();
                  foreach ($dt->getElements() as $t) {
                ?>
                <div class="col-12 form-group">
                    <input type="radio" class="form-checkin" name="delivery" value="<?= $t->id ?>"
                        <?= $cart->type_delivery == $t->id ? "checked='checked'" : "" ?>
                    />
                    <label for="deliverynal" style="color:black;"><?= $t->name ?></label>
                </div>

                <?php } ?>

            </div>
        </div>
        <div class="col-4">
            <div class="col-12 form-group" style="margin-top: 33px;">
                <span class="choosedelivery">Комментарий к заказу:</span>
            </div>


            <div class="col-12 form-group">
                <p class="yourcomment">Введите ваш комментарий:</p>
                <textarea class="comment" name="comment" placeholder="Текст комментария" ><?= $cart->text_comment ?></textarea>

            </div>

        </div>
        <div class="col-12 form-group">
            <button class="continue" order_id="<?= $cart->id ?>">Продолжить</button>
        </div>
    </div>
    <div class="row">
        <div class="col-12 contact-info" style="border-top: 2px solid #f6f6f6; padding-top: 8px;">
            <span style="color:#ed1651;">3.</span>
            Подтверждение заказа
        </div>

    </div>
</main>

<script src="checkout_content_step_2.script.js" ></script>
