<?php
	SESSION_START();
  
  include_once("./Model/common.php");
  
  
  include_once(SITE_ROOT."Model/user.php");
  include_once(SITE_ROOT."mailer/send.php");
  include_once(SITE_ROOT."Model/Log.php");

	$email = $_GET["email"];
  $return_url= @$_GET["return_url"];

	$user = (new User())->loadByEmail($email);

	if ($user == null)
	{
	    Log::insert(2,0,1,"Пользователь $email не найден");
		header("location:login.php?error=".urlencode("Пользователь не найден!"));
		exit();
	}
	
	if ($user->userPwd )
	{
        Log::insert(6,0,1,"Пользователь $email запросил восстановление пароля");
    
        echo SendEmail("Восстановление пароля SuperShop", $user->email,
        "Ваш пароль $user->userPwd"
        );
    
        header("location:login.php?error=".urlencode("Пароль отправлен на $email!"));
		exit();
	}

	
	

?>