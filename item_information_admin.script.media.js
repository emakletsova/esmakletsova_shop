//:
//-получить картинку
//++событие js = клик
//++"слыш, файл есть? а если найду?"
//++почитать полученное
//+временно сохранить в браузере
//
//+отобразить на странице
//-пометить для отправки на сервер при сохранении
//- сохранить на сервере
//--прочитать
//--положить в папку
//--сохранить в базе пути/связи
$(document).ready(function() //DOMContentLoaded
{
	function GetImagePlace(src)
	{
		return $('<div class="col-3 product-item"/>')
			.append(
				$('<div class="row"/>')
					.append(
						$('<div class="col-12 img-product-kat"/>')
							.append(
								$('<img class="product-image newphoto"/>').attr('src', src)
							)
					)
					.append(
						$('<div class="col-12"><span class="change-img">Изменить</span></div>')
					)
					.append(
						$('<div class="col-12"><span class="delete-img">Удалить</span></div>')
					)
			);
	};

	function GetNewImage(el)
	{
		var $placeholder = $(el).parents('.product-item').find('.img-product-kat');

		var input = document.createElement('input');
		input.type = 'file';

		input.onchange = e => { //function(e) {

			// getting a hold of the file reference
			var file = e.target.files[0];

			// setting up the reader
			var reader = new FileReader();
			reader.readAsDataURL(file);

			// here we tell the reader what to do when it's done reading...
			reader.onload = readerEvent => { //function(readerEvent) {
				var content = readerEvent.target.result; // this is the file content

				$placeholder.parents('.product-item')
					.before(GetImagePlace(content));


				if (window.deletephotoelement != null) {
					window.deletephotoelement.find('.delete-img').click();
					window.deletephotoelement = null;
				}
			};

		};

		input.click();

	}

	$('.add-img').on('click', function()
	{
		window.deletephotoelement = null;
		GetNewImage(this);
	});

	$('.product-list-information').on('click', '.delete-img', function() {
		var $placeholder = $(this).parents('.product-item').find('.img-product-kat');
		$placeholder.find('img.product-image').addClass("deleteimage");
		$placeholder.parents('.product-item').hide();
	});

	$('.product-list-information').on('click', '.change-img', function() {
		window.deletephotoelement = $(this).parents('.product-item');
		GetNewImage($('.add-img'));
	});
	window.deletephotoelement = null;
	/*
		img.newphoto
		img.deletephoto
	*/
	
});
