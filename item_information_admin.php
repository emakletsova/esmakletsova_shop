<?php 
	SESSION_START();
	include_once("./Model/common.php");
	
	include_once(SITE_ROOT."Model/product.php");
	include_once(SITE_ROOT."Model/category.php");
	include_once(SITE_ROOT."Model/user.php");
	User::checkSellMan();
	User::checkSuperAdmin();
	
	
	$PRODUCT_ID = $_GET["PRODUCT_ID"];
	
	$product = new Product($PRODUCT_ID);
	$category = new Category($product->category_id);
	
	
	
?>
<?php

	$page_title = "Просмотр товара";	
	
	include("admin.part.head.php");
	
	include("admin.part.leftmenu.php");
	
	include("item_information_admin.content.php");
	
	include("admin.part.footer.php");
?>