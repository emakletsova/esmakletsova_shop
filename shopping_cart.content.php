<?php

?>
<style>
    .product-image {
        height: 100%;
        max-height: 138px;
        max-width: 170px;
        max-width: 100%;
    }
    .linecart>.row>div
    {
        display:flex; align-items: center;
    }

</style>
	<main class="container shoppingcart">
		  <div class="row">
			<div class="col-12 cart-title">
				КОРЗИНА
			</div>
		</div> 
			
		<div class="row" style="font-family: Supermolot Light; font-size: 14px; color: #999999;">
			<div class="col-12 headlinecart">
			  <div class="row">
				 <div class="col-4" style="padding-left: 20px;">
				    Товар
				 </div>
				 <div class="col-2">
				   Доступность
				 </div>
				 <div class="col-2">
				   Стоимость
				 </div>
				 <div class="col-2">
				   Количество
				 </div>
				 <div class="col-2">
				   Итого
				 </div> 
		     </div>
			</div>
	   </div>
        <?php
        if ($cart->lines != null)
        foreach ($cart->lines as $line)
        {
        ?>
		<div class="row">
		   <div class="col-12 linecart" pid="<?= $line->product->id ?>" ptype="<?= $line->product_type; ?>">
			  <div class="row">
			    <div class="col-2">
				<img src="<?= $line->product->getMedias()[0]->path; ?>" class="product-image" />
			    </div>
			   <div class="col-2" style="font-family: Supermolot Light; font-size: 18px;">
			     <?= $line->product->name; ?>
			   </div>
			   <div class="col-2" style="font-family: Supermolot Light; font-size: 14px; color:#ed1651;">
			     <?= $line->product->inStock==1 ? "Eсть в наличии" : "нет на складе" ; ?>
			   </div>
			   <div class="col-2 productcartprice" style="font-family: Supermolot Light Italic; font-size: 18px; color:#5d5d5d;">
			     <?= $line->product->getPrintPrice(); ?>руб.
			   </div>
			   <div class="col-2" style="display:flex; align-items: center;">
				 <span class="minus"
                         onclick="if ($(this).siblings('.quantity').text()>0){$(this).siblings('.quantity').text(+$(this).siblings('.quantity').text()-1);} else return false;">-</span>
                   <span class="quantity" ><?= $line->quantity; /**/ ?></span>
                   <span class="plus"
                         onclick="$(this).siblings('.quantity').text(+$(this).siblings('.quantity').text()+1)">+</span>
			   </div>
			   <div class="col-1 pricetotal">
                   <?= $nombre_format($line->countSum()); /*функция расчёта суммы по строке заказа*/ ?>руб.
			   </div>
			   <div class="col-1" style="">
                   <img src="img/image_x.png" class="image-x" style="height: 17px;display: block;" />
			   </div>
			  </div>
		  </div>
		</div>
        <?php } ?>
		<div class="row">
		   <div class="col-12 linecart">
		      
		   </div>
		</div>
		<div class="row">
		   <div class="offset-8 col-4 pricetotalcart">
		     <span class="col-2" style="font-size: 24px; padding-right: 7px; padding-left: 25px;">Итого:</span>
               <span class="col-2" style=" font-size: 30px;"><?= $nombre_format($cart->getLinesSum()); ?>руб. </span>
		      
		   </div>
		</div>
		<div class="row" style="padding-top: 14px;">
		   <div class="col-2" style="background-color: black; margin-left: 28px; ">
		    <a href="super_shop.php" style="font-family: Supermolot Light; font-size:14px; color: white; text-decoration: none; text-align: center; display: block; margin-top: 11px;"> Вернуться к покупкам</a>
		      
		   </div>
		   <a href="checkout.php" class="offset-6 col-3">
		     
			 <input type="submit" value="Оформить заказ" class="issueorder">
			
		   </a>
		</div>
		<div class="row">
			<div style="height:20px;">
			</div>
		</div>
	</main>

<script>
    $(document).ready(function () {
        $('.image-x').on('click', function () {

            if (!confirm( "Удалить?" ))
                return false;
            var param = {};
            param["product_id"] = $(this).parents('.linecart').attr('pid');
            $.post(
                "/shop/Action/product.removeFromCart.php",
                param,
                function (data) {
                    console.log(data);
                    location.reload();
                },
                "text"
            )
                .fail(function (data) {
                    alert("Не получилось :(\r\n" + data);
                });

        });

        $('.quantity').on('DOMSubtreeModified',function(){

            var param = {};
            param["product_id"] = $(this).parents('.linecart').attr('pid');
            param["product_type"] = $(this).parents('.linecart').attr('ptype');

            param["quantity"] = $(this).text();
            if (param["quantity"] =="") return;
            $.post(
                "/shop/Action/product.changeAmountCart.php",
                param,
                function (data) {
                    console.log(data);
                    location.reload();
                },
                "text"
            )
                .fail(function (data) {
                    alert("Не получилось :(\r\n" + data);
                });

        })

    });
</script>



<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>

