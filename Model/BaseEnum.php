<?php
  include_once(SITE_ROOT."Model/common.php");
  include_once(SITE_ROOT."Model/BaseEnumElement.php");
  
  
  
  class BaseEnum
  {
    public  $elements = null;
    public function __construct($enum_name)
    {
  
      $sql =  "SELECT * FROM $enum_name";
      $db = new DB();
      $enum_db =  $db->query($sql);
      if ($this->elements == null) {
        $this->elements = array();
        while ($el = $enum_db->fetch_assoc()) {
          //
          $l = new BaseEnumElement();
          //
          $l->id = $el["id"];
          $l->name = $el["name"];
          array_push($this->elements, $l);
        }
      }
    }
    
    public function getElements()
    {
      return $this->elements;
    }
    
    public function getElementById($id)
    {
      $key = array_search($id, array_column($this->elements, 'id'));
      return $this->elements[$key];
    }
  }