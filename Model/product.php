<?php
  include_once(SITE_ROOT."Model/common.php");
	include_once(SITE_ROOT."Model/db.php");
  include_once(SITE_ROOT.'/Model/media.php');
  include_once(SITE_ROOT.'/Model/category.php');
	class Product
	{
		public $id;
		public $name;
		public $description;
    public $price = 0;
		public $inStock = 0;
		public $medias = null;
		public $types = null;
		public $category_id = 0;
		public $category = null;
		public $newProduct = 0;
		public $popularProduct = 0;
		public $saleProduct = 0;
    public $fullPrice = 0;
    public $saleSum = 400;
    
    public function getPrintPrice()
    {
      return number_format($this->price, 0, ',', ' ');
    }
    
    
    public function getPrintPriceFull()
    {
      return number_format($this->fullPrice, 0, ',', ' ');
    }
    
    public function getMedias($addEmptyMedia = true)
    {
        if ( $addEmptyMedia && sizeof($this->medias)==0)
        {
          return [ Media::GenerateEmptyMedia() ];
        }
      
      return $this->medias;
    }
    
    public function __construct($id = -1)
		{
			if (isset($id) && $id>0)
				$this->loadById($id);
		}
		
		public function loadById($id)
		{
			$sql = "select * from product where id = '$id'";
			
			$p = (new DB())->query($sql)->fetch_assoc();
			if ($p == NULL)
			{
				return null;
			}
			$this->name = $p["name"];
			$this->description = $p["description"];
			$this->price = $p["price"];
			$this->inStock = $p["in_stock"];
			$this->id = $id;
			$this->category_id = $p["category_id"];
			$this->newProduct = $p["new_product"];
			$this->popularProduct = $p["popular_product"];
      $this->saleProduct = $p["sale_product"];
      //$this->saleSum = $p["sale_sum"];
      if ($this->saleProduct) {
        $this->fullPrice = $this->price;
        $this->price -= $this->saleSum;
      }
      else
        $this->fullPrice = $this->price;
        
      $this->fillMedia();
			$this->fillTypes();
			$this->fillCategory();
		}
		
		public function insertDB()
		{
			$sql = "INSERT INTO product (id, name, description, price, in_stock, category_id, new_product, popular_product, sale_product) VALUES (NULL, '"
			. addslashes($this->name) . "', '" . addslashes($this->description) . "', $this->fullPrice, $this->inStock,$this->category_id, $this->newProduct, $this->popularProduct, $this->saleProduct)";
			$db = new DB();
			//echo $sql;
			if($db->query($sql) != NULL)
			{
				$this->id = $db->lastId;
				
				return $this;
			}
			else return null;			
		}
		
		public function updateDB()
		{
			if (!isset($this->id))
			{	
				return $this->insertDB();
			}
			
			$sql = "UPDATE product
                SET name='" . addslashes($this->name) . "',
                    description='" . addslashes($this->description) . "',
                    price='$this->fullPrice',
                    in_stock='$this->inStock',
                    category_id='$this->category_id' ,
                    new_product='$this->newProduct',
                    popular_product='$this->popularProduct',
                    sale_product='$this->saleProduct'
                where id='$this->id'";
			
			$this->saveTypes();
			//var_dump($sql);
			//die();
			return (new DB())->query($sql);
		}
		function chkDelete()
    {
        $sql = "SELECT * FROM order_product_relation inner join orders on order_id=id
                where product_id=$this->id and (orders.status>2 or orders.status<5)";
  
        if((new DB())->query($sql)->fetch_assoc() == null)
          return true;
        else return false;
    }
		function deleteDB()
		{
		  if (!$this->chkDelete()) return false;
		  
			$sql = "DELETE FROM product WHERE id='$this->id'";
			$db = new DB();
			return $db->query($sql);
		}

		public function addMediaToProduct($media)
		{
			if (!isset($this->id) || !($this->id>0))
				return false;
			if (!isset($media->id) || !($media->id>0))
				return false;
			
			$db = new DB();
			$sql = "SELECT * FROM product_media_relation WHERE product_id=$this->id AND media_id=$media->id";
			
			if($db->query($sql)->fetch_assoc() == null)
			{
				$sql = "INSERT INTO product_media_relation (product_id,media_id) VALUES ('$this->id','$media->id')";
				$db->query($sql);
			}
			return true;
		}
		
		public function unlinkMediaFromProduct($media)
		{
			if (!isset($this->id) || !($this->id>0))
				return false;
			if (!isset($media->id) || !($media->id>0))
				return false;
			
			$db = new DB();
			
			$sql = "delete from product_media_relation where product_id = '$this->id' and media_id = '$media->id'";
			$db->query($sql);
			
			
			$sql = "SELECT * FROM product_media_relation WHERE media_id='$media->id'";
			if($db->query($sql)->fetch_assoc() == null)
			{
				$media->deleteDB();				
			}
			
			return true;
		}
		
		/*
		select * from product_media_relation 
		left join media on product_media_relation.media_id = media.id
		where product_media_relation.product_id =
		*/
		public function fillMedia()
		{
			if (!isset($this->id) || !($this->id>0))
				return false;
			
			
			$sql = "select * 
			from product_media_relation 
			left join media 
					on product_media_relation.media_id = media.id 
			where product_media_relation.product_id = $this->id";
			
			$db = new DB();
			$db_media = $db->query($sql);//выполняем запрос
			$this->medias = array();
			while ($media = $db_media->fetch_assoc())//пока есть данные в запросе
			{
				array_push($this->medias,Media::CreateFromDB($media));
			}
			
			return true;
		}
		
		public function fillTypes()
		{
			if (!isset($this->id) || !($this->id>0))
				return false;
			
			
			$sql = "select * from product_type where product_type.product_id = $this->id";
			
			$db = new DB();
			$db_types = $db->query($sql);
			$this->types = array();//[]
			while ($type = $db_types->fetch_assoc())
			{
				
				$this->types[] = $type["type_name"];
			}
						
			return true;
		}
		
		
		public function clearTypes()
		{
			$sql = "DELETE FROM product_type WHERE product_id=$this->id";
			$db = new DB();
			return $db->query($sql);
		}
		
		public function saveTypes()
		{
			if (!isset($this->id) || !($this->id>0) || $this->types == null)
				return false;
			
			
			
			
			$db = new DB();
			foreach($this->types as $t)
			{
				if ($t=="")
					continue;
				
				$sql = "SELECT * FROM product_type WHERE product_id=$this->id AND type_name='$t'";
				
				if($db->query($sql)->fetch_assoc() == null)
				{
					$sql = "INSERT INTO product_type (product_id,type_name) VALUES ('$this->id','$t')";
					$db->query($sql);
				}
			}
			return true;
		}
		
		public function fillCategory()
		{
			if (!isset($this->id) || !($this->id>0))
				return false;
			
			
			$sql = "select * from category where id = '$this->category_id'";
			
			$db = new DB();
			$db_category = $db->query($sql);
			$category = $db_category->fetch_assoc();
			
			$this->category = Category::CreateFromDB($category);
						
			return true;
		}
		
		//создать новый экземпляр класса медиа из выполненного запроса к бд
		public static function CreateFromDB($db_product)
		{
			$product = new Product();
			
			$product->id = $db_product["id"];
			$product->name = $db_product["name"];
			$product->description = $db_product["description"];
			$product->price = $db_product["price"];
			$product->inStock = $db_product["in_stock"];
			$product->category_id = $db_product["category_id"];
			$product->newProduct = $db_product["new_product"];
			$product->popularProduct = $db_product["popular_product"];
			$product->saleProduct = $db_product["sale_product"];
      
      
      if ($product->saleProduct) {
        $product->fullPrice = $product->price;
        $product->price -= $product->saleSum;
      }
      else
        $product->fullPrice = $product->price;
      
			$product->fillMedia();
			$product->fillTypes();
			$product->fillCategory();
			
			return $product;
		}
		public static function getNewProduct()
		{
			
			$sql = "select * from product where new_product = 1";
			
			$db = new DB();
			$db_prod = $db->query($sql);
			$new_products = array();
			while ($p = $db_prod->fetch_assoc())
			{
				array_push($new_products, Product::CreateFromDB($p));
			}
			return $new_products;
		}
    
    public static function getPromoProduct()
    {
      
      $sql = "select * from product limit 5";
      
      $db = new DB();
      $db_prod = $db->query($sql);
      $new_products = array();
      while ($p = $db_prod->fetch_assoc())
      {
        $p["description"] = implode(".", array_slice( explode(".", $p["description"]), 0, 2))
        . ".";
        array_push($new_products, Product::CreateFromDB($p));
      }
      return $new_products;
    }
		public static function getPopularProduct()
		{
		
			
			$sql = "select * from product where popular_product = 1";
			
			$db = new DB();
			$db_prod = $db->query($sql);
			$pop_products = array();
			while ($p = $db_prod->fetch_assoc())
			{
				array_push($pop_products, Product::CreateFromDB($p));
			}
			return $pop_products;
		}
    public static function getSaleProduct()
    {


        $sql = "select * from product where sale_product = 1";

        $db = new DB();
        $db_prod = $db->query($sql);
        $sale_products = array();
        while ($p = $db_prod->fetch_assoc())
        {
            array_push($sale_products, Product::CreateFromDB($p));
        }
        return $sale_products;
    }


    public function getFlag()
    {
        if ($this->newProduct)
            $img = "img/status_new.png";
        if ($this->popularProduct)
            $img = "img/status_hot.png";
        if ($this->saleProduct)
            $img = "img/status_sale.png";

        return isset($img) ? "<img src='$img' class='product-flag' />" : "";
    }
	}



	//var_dump(new Product(29));
?>