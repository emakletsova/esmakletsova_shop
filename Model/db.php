<?php
  include_once(SITE_ROOT."Model/common.php");

	class DB
	{
		static $serverAddress = MYSQL_SERVER_ADDR;
		static $username = MYSQL_USER;
		static $password = MYSQL_PWD;
		static $dbname = MSQL_DBNAME;
		private $db_link = NULL;
		public $lastId = null;
		
		function connect()
		{
			if ($this->db_link != NULL)
				return $this->db_link;
			
			$this->db_link = mysqli_connect(self::$serverAddress, self::$username, self::$password, self::$dbname);
            mysqli_set_charset($this->db_link,"utf8");

			if (!$this->db_link) {
				echo "Ошибка: Невозможно установить соединение с MySQL." . PHP_EOL;
				echo "Код ошибки errno: " . mysqli_connect_errno() . PHP_EOL;
				echo "Текст ошибки error: " . mysqli_connect_error() . PHP_EOL;
				die();
			}	
			
			return $this->db_link;
		}
		
		public function query ($sql)
		{
			$this->lastId = null;
			
			$result = mysqli_query ( $this->connect(), $sql);
			$this->lastId = mysqli_insert_id($this->connect());
			if ($result == false)
			{
				throw new RuntimeException( "Текст ошибки error: " . mysqli_error($this->db_link) );
				die();
			}
			//var_dump ($sql);
			//var_dump ($result);
			//$this->disconnect();
			
			return $result;
		}
				
		function disconnect()
		{
			if ($this->db_link != NULL)
				mysqli_close($this->db_link);
			$this->db_link = null;
		}
		
		function __destruct()
		{
			$this->disconnect();	
		}
	}

?>