<?php
  include_once(SITE_ROOT."Model/common.php");
  include_once(SITE_ROOT."Model/BaseEnum.php");
  
  
  class LogActionType  extends BaseEnum
  {
 		public function __construct()
    {
      parent::__construct("transaction_action_type");
    }

  }