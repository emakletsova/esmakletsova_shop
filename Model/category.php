<?php
//SITE_ROOT => "C:\Repos\esmakletsova_shop"
  include_once(SITE_ROOT."Model/common.php");
  include_once(SITE_ROOT."Model/db.php");
	include_once (SITE_ROOT."Model/product.php");
	class Category
	{
		public $id;
		public $name;
		public $catDescription;
		public $shotDescription;
		
		public function __construct($id = -1)
		{
			if (isset($id) && $id>0)
				$this->loadById($id);
		}
		
		public function loadById($id)
		{
			$cat = $this->searchDB($id)->fetch_assoc();
			if ($cat == NULL)
			{
				return null;
			}
			$this->name = $cat["name"];
      $this->catDescription = $cat["category_description"];
      $this->shotDescription = $cat["shot_description"];
			$this->id = $id;
		}
		
		
		public function loadOrInsertByName($name)
		{
			$cat = $this->searchDB_name($name)->fetch_assoc();
			if ($cat == NULL)
			{
				$this->name = $name;
				$this->insertDB() ;
				return null;
			}
			$this->name = $cat["name"];
			$this->id = $cat["id"];
		}
		
		public function insertDB()
		{
			$sql = "INSERT INTO category (id, name, category_description, shot_description)
        VALUES (NULL, '$this->name', '$this->catDescription', '$this->shotDescription')";
			
			$db = new DB();			
			if($db->query($sql) != NULL)
			{
				$this->id = $db->lastId;
				return $this;
			}
			else return null;			
		}
		
		function searchDB($id)
		{		 		
			$sql =  "SELECT * FROM category WHERE id='$id'";
			$db = new DB();
			return $db->query($sql);
		}
		function searchDB_name($name)
		{		 		
			$sql =  "SELECT * FROM category WHERE name='$name'";
			$db = new DB();
			return $db->query($sql);
		}
		
		public function updateDB()
		{
			if (!isset($this->id))
			{	
				return $this->insertDB();
			}
			
			$sql = "UPDATE category SET name='$this->name' category_description='$this->catDescription' shot_description='$this->shotDescription' where id='$this->id'";
			
			$db = new DB();
			return $db->query($sql);
		}
		
		public function deleteDB()
		{
			if (!isset($this->id))
			{	
				return FALSE;
			}
			$sql = "DELETE FROM category WHERE id='$this->id'";
			$db = new DB();
			return $db->query($sql);
		}
		
		public static function CreateFromDB($db_cat)
		{
			$cat = new Category();
			$cat->id = $db_cat["id"];
			$cat->name = $db_cat["name"];
      $cat->catDescription = $db_cat["category_description"];
      $cat->shotDescription = $db_cat["shot_description"];
			return $cat;
		}
		
		
		
		public function getProductCount()
		{
			if (!isset($this->id))
			{	
				return 0;
			}
			
			$sql = "select count(*) total from product where category_id='$this->id'";
			
			$db = new DB();
			return $db->query($sql)->fetch_assoc()["total"];
		}
		
		 
		function getProductList($page = 0, $limit = 0)
		{
			if (!isset($this->id))
			{	
				return false;
			}
			$offset = $page * $limit;
			if($limit == 0)
				$sql = "select * from product where category_id='$this->id'";
			else $sql = "select * from product where category_id='$this->id' limit $offset, $limit";
			
			
			$db = new DB();
			
			$db_prod = $db->query($sql);
			$products = array();
			while ($p = $db_prod->fetch_assoc())
			{
				array_push($products, Product::CreateFromDB($p));
			}
			return $products;
		}
    
    public static function getAllProductList($page = 0, $limit = 0)
    {
      $offset = $page * $limit;
      if($limit == 0)
        $sql = "select * from product";
      else $sql = "select * from product limit $offset, $limit";
      
      
      $db = new DB();
      
      $db_prod = $db->query($sql);
      $products = array();
      while ($p = $db_prod->fetch_assoc())
      {
        array_push($products, Product::CreateFromDB($p));
      }
      return $products;
    }
		
		function getProductListExceptCurrent($currProductId, $page = 0, $limit = 0)
		{
			if (!isset($this->id))
			{	
				return false;
			}
			$offset = $page * $limit;
			if($limit == 0)
				$sql = "select id from product where category_id='$this->id' and id<>'$currProductId'";
			else $sql = "select id from product where category_id='$this->id' and id<>'$currProductId' limit $offset, $limit";
			
			
			$db = new DB();
			
			$db_prod = $db->query($sql);
			$products = array();
			while ($p = $db_prod->fetch_assoc())
			{
				array_push($products,new Product($p['id']));
			}
			return $products;
		}
		
		
		public static function getCategoryList()
		{
			$sql =  "SELECT * FROM category";
			$db = new DB();
			$db_cats = $db->query($sql);
			$cats = array();
			while ($cat = $db_cats->fetch_assoc())
			{
				array_push($cats,new Category($cat['id']));
			}
			return $cats;
		}
		
		public static function getAmountProduct()
		{
			$sql = "SELECT category.id category_id, category.name, product.category_id as c_id, count(product.id) AS amount FROM product right join category on category.id=product.category_id GROUP BY product.category_id";
			$db = new DB();
			return $db->query($sql);
			
		}
	}

	
?>