<?php
  include_once(SITE_ROOT."Model/common.php");
  include_once(SITE_ROOT."Model/order.php");

    include_once "db.php";

    class User
    {
      public $id;
      public $userName;
      public $userPwd;
      public $role;
      public $roleName;
      public $email;
      public $userPhone;
      public $userTown;
      public $userStreet;
      public $userHouse;
      public $userApartment;
  
  
      public function __construct($id = -1)
        {
            if (isset($id) && $id>0)
                $this->loadById($id);
        }

        public function loadById($id)
        {
          $sql = "select *, user.id user_id from user left join user_role on user.role=user_role.id where user.id = '$id'";

          $u = (new DB())->query($sql)->fetch_assoc();
          if ($u == NULL)
          {
              return null;
          }
          $this->userName = $u["user_name"];
          $this->id = $u["user_id"];
          $this->email = $u["email"];
          $this->role = $u["role"];
          $this->roleName = $u["name"];
          $this->userPwd = $u["password"];
          $this->userPhone = $u["number"];
          $this->userTown = $u["town"];
          $this->userStreet = $u["street"];
          $this->userHouse = $u["house"];
          $this->userApartment = $u["apartment"];
			

        }

        public function loadByEmail($email)
        {
            $sql = "select  *, user.id user_id from user left join user_role on user.role=user_role.id where user.email = '$email'";
 
            $u = (new DB())->query($sql)->fetch_assoc();
            if ($u == NULL)
            {
                return null;
            }
			//var_dump($u);
            $this->userName = $u["user_name"];
            $this->id = $u["user_id"];
            $this->email = $u["email"];
            $this->role = $u["role"];
            $this->roleName = $u["name"];
            $this->userPwd = $u["password"];
            $this->userPhone = $u["number"];
            $this->userTown = $u["town"];
            $this->userStreet = $u["street"];
            $this->userHouse = $u["house"];
            $this->userApartment = $u["apartment"];
            return $this;

        }

        public function insertDB()
        {
            $sql = "INSERT INTO user (id, user_name, password, email, role, number, town, street, house, apartment)
                      VALUES (NULL, '$this->userName', '$this->userPwd', 
                      '$this->email', '$this->role','$this->userPhone', '$this->userTown', '$this->userStreet','$this->userHouse','$this->userApartment')";
            $db = new DB();
            //echo $sql;
            if($db->query($sql) != NULL)
            {
                $this->id = $db->lastId;
                return $this;
            }
            else return null;
        }

        public function updateDB()
        {
            if (!isset($this->id))
            {
                return $this->insertDB();
            }

            $sql = "UPDATE user SET user_name='".addslashes($this->userName)."', password='$this->userPwd',
                      email='$this->email', role = '$this->role', number = '$this->userPhone', town = '$this->userTown', street = '$this->userStreet', house = '$this->userHouse', apartment = '$this->userApartment'
                      where id='$this->id'";


            return (new DB())->query($sql);
        }

        function deleteDB()
        {
            $orderList = Order::getUserOrdersList($this->id);
            foreach ($orderList as $order)
              $order->deleteDB();
            
            
            $sql = "DELETE FROM user WHERE id='$this->id'";
            $db = new DB();
            return $db->query($sql);
        }


        public function printUserInfo()
        {
            if (isset($this->id))
            {
                return "<div>$this->userName</div>";
            }

            return "";
        }
		public static function getUsersList()
		{
			$sql =  "SELECT * FROM user";
			$db = new DB();
			$db_users = $db->query($sql);
			$users = array();
			while ($user = $db_users->fetch_assoc())
			{
				array_push($users,new User($user['id']));
			}
			return $users;
		}



//проверка на авторизованность посетителя
        public static function IsCurrUserAuth()
        {
            return isset($_SESSION["CurrentUserId"]);
        }
//установка авторизованности
        public static function SetCurrUserAuth($CurrentUserId)
        {
            $_SESSION["CurrentUserId"] = $CurrentUserId;
        }
//если авторизован - отдаём текущего юзера
        static $_currUser = null;
        public static function GetCurrUser()
        {
            if (!self::IsCurrUserAuth())
                throw new Exception("GetCurrUser() у неавторизованного!");

            if (self::$_currUser == null)
                self::$_currUser = new User($_SESSION["CurrentUserId"]);

            return self::$_currUser;
        }
        public static function UnsetCurrUserAuth()
        {
            unset($_SESSION["CurrentUserId"]);
        }
        
        public static function checkSuperAdmin()
        {
          if (!User::IsCurrUserAuth())
          {
            header("location:login.php");
            exit();
          };
          
          
          if (User::GetCurrUser()->role != 10) {
            header("location:order_list_admin.php");
            exit();
          }
        }
    
    
        public static function checkSellMan()
        {
          if (!User::IsCurrUserAuth())
          {
            header("location:login.php");
            exit();
          };
          
          
          if (User::GetCurrUser()->role != 5 && User::GetCurrUser()->role != 10) {
            header("location:super_shop.php");
            exit();
          }
        }
    }

?>