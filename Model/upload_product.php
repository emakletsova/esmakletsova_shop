<?php


require './../vendor/autoload.php';
include_once("./../Model/common.php");
require SITE_ROOT.'Model/product.php';

use PhpOffice\PhpSpreadsheet\IOFactory;

$baseFolder = SITE_ROOT.'uploader';
$saveFolder = SITE_ROOT.'files';

$folderList = scandir($baseFolder);




function loadProductFromExcelLine($line) {

	global $category;
	
    $product = new Product();
	$product->name = $line['B'];
	$product->description = $line['C'];
  $product->price = floatval(str_replace(' ','',$line['E']));
  $product->fullPrice = floatval(str_replace(' ','',$line['E']));
	$product->types = explode(', ', $line['D']);
	$product->inStock = 1;
	$product->newProduct = 0;
  $product->popularProduct = 0;
  $product->saleProduct = 0;
	
	$product->category_id = $category->id;
	
	$article = $line['A'];
	
	//var_dump ($product);
	//return;
	$product->updateDB();
	$product->saveTypes();
	
	
	
	global $loadPath;
	global $saveFolder;
	
	
	$fileList = scandir($loadPath);
	
	$checkFilename = function($name) use ($article)
	{
		return preg_match('/^'.$article.'_*.\.jpg$/', $name) || preg_match('/^'.$article.'_*.\.png$/', $name);
	};
	//filter file array to find medias
	$found = array_filter($fileList, $checkFilename);
	
	//var_dump($found);
	
	foreach ($found as $filename)
	{
		$pathFrom = $loadPath.'/'.$filename;
		$pathTo = $saveFolder.'/'.$product->id.'_'.$filename;
		
		copy($pathFrom,$pathTo);
		
		$media = new Media();
		$media->path = str_replace("..",".",$pathTo);
		$media->name = $filename;
		$media->type = 1;
		$media->updateDB();
		$product->addMediaToProduct($media);
		
	}	
	
	
	//$product->
	//var_dump ($product);
}


foreach($folderList as $folder)
{
	$category_name = $folder;
	if ($category_name=="." || $category_name=="..")
	{
		continue;
	}
	
	$category = new Category();
	$category->loadOrInsertByName($category_name);
	$loadPath = $baseFolder. "/". $folder;
	$inputFileName = $loadPath . '/Товары.xlsx';
	
	//echo 'Loading file ' . pathinfo($inputFileName, PATHINFO_BASENAME) . ' using IOFactory to identify the format';
	echo "загрузка из $inputFileName.. ";
	$spreadsheet = IOFactory::load($inputFileName);
	$sheetData = $spreadsheet->getActiveSheet()->toArray(null, true, true, true);
	//var_dump($sheetData); 

	array_shift($sheetData);//$a = [1,2,3] -> return 1; $a = [2,3]

	//loadProductFromExcelLine($sheetData[0]);
	array_walk($sheetData, 'loadProductFromExcelLine');
	echo "готово " . count($sheetData) . " товаров<BR/>" ;
	
}
?>