<?php
  include_once(SITE_ROOT."Model/common.php");
  include_once(SITE_ROOT."Model/BaseEnum.php");
  
  
  class OrderStatus extends BaseEnum
  {
 		public function __construct()
    {
      parent::__construct("order_status");
    }

  }