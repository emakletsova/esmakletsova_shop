<?php
  include_once(SITE_ROOT."Model/common.php");
  include_once SITE_ROOT.'Model/db.php';
  include_once(SITE_ROOT."Model/product.php");

  include_once SITE_ROOT.'Model/user.php';
	include_once SITE_ROOT.'Model/order.php';
	
	class OrderLine
	{
		public $product = null;
    public $quantity = 1;
    public $order_id = -1;
    public $product_type = "";
    
    public function __construct($productId = -1, $order_id = -1, $product_type = "")
    {
      $this->order_id = $order_id;
      if (isset($productId) && $productId>0)
        $this->product = new Product($productId);
      //if (isset($product_type) && $product_type!="")
      $this->product_type = $product_type;
  
    }
		
		public function countSum()
		{
			if ($this->product == null)
				return 0;
			return $this->quantity * $this->product->price;
		}


    public function updateDB()
    {

      if ($this->quantity==0)
          return $this->deleteDB();

      $db = new DB();
      $sql = "SELECT * FROM order_product_relation
              WHERE order_id='$this->order_id'
                and product_id='".($this->product)->id."'
                and product_type='$this->product_type'";
      if($db->query($sql)->fetch_assoc() == null)
      {
          $sql = "INSERT INTO order_product_relation(order_id, product_id, amount, product_type)
              VALUES ('$this->order_id', '".($this->product)->id."', '$this->quantity', '$this->product_type')";

          return $db->query($sql);
      }

      $sql = "UPDATE order_product_relation
              SET amount='$this->quantity'
              where order_id='$this->order_id' and product_id='".($this->product)->id."'
                and product_type='$this->product_type'";

      return $db->query($sql);
    }

    function deleteDB()
    {
        $sql = "DELETE FROM order_product_relation
              WHERE order_id='$this->order_id'
                and product_id='".($this->product)->id."'
                and product_type='$this->product_type'";
        $db = new DB();
        return $db->query($sql);
    }


	}
	
?>