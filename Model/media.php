<?php
  include_once(SITE_ROOT."Model/common.php");
	include_once(SITE_ROOT."Model/db.php");

	class Media
	{
		public $id;
		public $type = 1;
		public $description;
		public $path;
		
		public function __construct($id = -1)
		{
			if (isset($id) && $id>0)
				$this->loadById($id);
		}
		
		public function loadById($id)
		{
			$med = $this->searchDB($id)->fetch_assoc();
			if ($med == NULL)
			{
				return null;
			}
			$this->type = $med["type"];
			$this->description = $med["description"];
			$this->path = $med["path"];
			$this->id = $id;
		}

		public function insertDB()
		{
			$sql = "INSERT INTO media (id, type, description, path) VALUES (NULL, '$this->type', '$this->description', '$this->path')";
			
			$db = new DB();			
			if($db->query($sql) != NULL)
			{
				$this->id = $db->lastId;
				return $this;
			}
			else return null;			
		}
		
		function searchDB($id)
		{		 		
			$sql =  "SELECT * FROM media WHERE id='$id'";
			$db = new DB();
			return $db->query($sql);
		}
		
		public function updateDB()
		{
			if (!isset($this->id))
			{	
				return $this->insertDB();
			}
			
			$sql = "UPDATE media SET type='$this->type', description='$this->description', path='$this->path' where id='$this->id'";
			
			$db = new DB();
			return $db->query($sql);
		}
		
		function deleteDB()
		{
			$sql = "DELETE FROM media WHERE id='$this->id'";
			$db = new DB();
			//: delete from disk
			if( $db->query($sql))
				return unlink($this->path);
		}
		
		//запись файла на диск
		public function writeFile($newFile)
		{
			return file_put_contents($this->path, $newFile);
		}
		
		//создать новый экземпляр класса медиа из выполненного запроса к бд
		public static function CreateFromDB($db_media)
		{
			$media = new Media();
			$media->id = $db_media["id"];
			$media->type = $db_media["type"];
			$media->path = str_replace("..",".",$db_media["path"]);
			$media->description = $db_media["description"];
			return $media;
		}
    
    public static function GenerateEmptyMedia()
    {
      $media = new Media();
      $media->path = "img/image-product.png";
      return $media;
    }
	}

	
?>