<?php

	include_once(SITE_ROOT."Model/db.php");
	$db = new DB();
	
	$sql = "DELETE FROM product WHERE 1=1";
	$db->query($sql);
	
	$sql = "DELETE FROM media WHERE 1=1";
	$db->query($sql);
	
	$sql = "DELETE FROM product_type WHERE 1=1";
	$db->query($sql);
	
	$sql = "DELETE FROM product_media_relation WHERE 1=1";
	$db->query($sql);

  $sql = "DELETE FROM category WHERE 1=1";
  $db->query($sql);

  $sql = "DELETE FROM order_product_relation WHERE 1=1";
  $db->query($sql);

  $sql = "DELETE FROM orders WHERE 1=1";
  $db->query($sql);


  $files = glob('../files/*'); // get all file names
  foreach($files as $file){ // iterate files
    if(is_file($file))
      unlink($file); // delete file
  }

	?>