<?php
  include_once(SITE_ROOT."Model/common.php");
  include_once(SITE_ROOT."Model/db.php");
  include_once(SITE_ROOT."Model/product.php");
  include_once(SITE_ROOT."Model/DeliveryType.php");
  include_once(SITE_ROOT."Model/OrderStatus.php");

  include_once SITE_ROOT."Model/user.php";
	include_once SITE_ROOT."Model/orderline.php";

	
	class Order
	{





	public $id;
	public $user_id;
    public $status;
    public $status_name;
    public $status_name_cust;
    public $type;
    public $lines = NULL;
    public $town;
    public $street;
    public $house;
    public $apartment;
    public $text_comment;
    public $type_delivery = 1;
    public $type_delivery_name;
    public $confirm_time;
		
		public function __construct($id = -1)
        {
            if (isset($id) && $id>0)
                $this->loadById($id);
            else{
              if (User::IsCurrUserAuth()) {
                $user = User::GetCurrUser();
                $this->town = $user->userTown;
                $this->street = $user->userStreet;
                $this->house = $user->userHouse;
                $this->apartment = $user->userApartment;
                
              }
            }
        }
		
		public function loadById($id)
		{
			$sql = "select * from orders where id = '$id'";

			$ord = (new DB())->query($sql)->fetch_assoc();
			if ($ord == NULL)
			{
				return null;
			}
      $this->user_id = $ord["user_id"];
      $this->status = $ord["status"];
      $this->status_name = (new OrderStatus())->getElementById( $this->status)->name;

      switch ($this->status)
      {
        case 1:
        case 2:
        case 3:
          $this->status_name_cust = "Ожидает доставки";
          break;
        case 4:
          $this->status_name_cust = "Доставлен";
          break;
        case 5:
          $this->status_name_cust = "Отменён";
          break;
      }

      $this->type = $ord["type"];
      $this->id = $id;


      $this->town = $ord["town"];
      $this->street = $ord["street"];
      $this->house = $ord["house"];
      $this->apartment = $ord["apartment"];
      $this->confirm_time = $ord["confirm_date"];

      $this->text_comment = $ord["text_comment"];
      $this->type_delivery = $ord["type_delivery"];
      
      $this->type_delivery_name = (new DeliveryType())->getElementById( $this->type_delivery)->name;
      
      
			$this->fillLines();
		}
		public function insertDB()
		{
			$sql = "insert into orders (id, user_id, status, type,  town, street, house, apartment, text_comment, type_delivery)
			values (NULL, '$this->user_id', '$this->status', '$this->type',
			        '$this->town', '$this->street','$this->house',
			        '$this->apartment', '$this->text_comment', '$this->type_delivery')";
			
			$db = new DB();
            //echo $sql;
            if($db->query($sql) != NULL)
            {
                $this->id = $db->lastId;
                return $this;
            }
            else return null;
		}

    public function updateDB()
    {
        if (!isset($this->id))
        {
            return $this->insertDB();
        }
        
        $sql = "UPDATE orders SET
                  town = '$this->town', street = '$this->street',
                  house = '$this->house', apartment = '$this->apartment', text_comment = '$this->text_comment',
                  type_delivery = '$this->type_delivery'
                where id='$this->id'";


        return (new DB())->query($sql);
    }
		public function setStatus($status)
		{
			$this->status = $status;
			if (!isset($this->id))
            {
                return $this->insertDB();
            }
			$sql = "update orders set
			status = '$this->status', type = '2'".
          ($status == 2 ? ( ", confirm_date = '".date("Y-m-d H:i:s")."'" ): "")
      .
      " where id='$this->id'";
			
			return (new DB())->query($sql);
		}
    public function setTypeDelivery($type_delivery)
    {
        $this->type_delivery = $type_delivery;
        if (!isset($this->id))
        {
            return $this->insertDB();
        }
        $sql = "update orders set
        type_delivery = '$this->type_delivery'
        where id='$this->id'";

        return (new DB())->query($sql);
    }
		
		public function deleteDB()
        {
  
          if ($this->lines != null)
            foreach ($this->lines as $line)
            {
              $line->deleteDB();
            }
          
            $sql = "DELETE FROM orders WHERE id='$this->id'";
            $db = new DB();
            return $db->query($sql);
        }
		
		public function fillLines()
        {
			if (!isset($this->id) || !($this->id>0))
				return false;
			
            $sql = "select * from order_product_relation 
			left join product on order_product_relation.product_id=product.id
			
			WHERE order_product_relation.order_id='$this->id'";
			
      $db = new DB();
      $db_line = $db->query($sql);
			$this->lines = array();
			while ($line = $db_line->fetch_assoc())
			{
				//
				$l = new OrderLine();
				//
				$l->product = Product::CreateFromDB($line);
        $l->quantity = $line["amount"];
        $l->order_id = $this->id;
        $l->product_type = $line["product_type"];
				array_push($this->lines,$l);
			}
						
			return true;
        }
		public static function CreateFromDB($db_order)
		{
			$order = new Order();
			$order->id = $db_order["id"];
      $order->user_id = $db_order["user_id"];
			$order->status = $db_order["status"];
			$order->type = $db_order["type"];
      $order->town = $db_order["town"];
      $order->street = $db_order["street"];
      $order->house = $db_order["house"];
      $order->apartment = $db_order["apartment"];
      $order->text_comment = $db_order["text_comment"];
      $order->type_delivery = $db_order["type_delivery"];
      $order->confirm_time = $db_order["confirm_date"];
      $order->type_delivery_name = (new DeliveryType())->getElementById( $order->type_delivery);
      $order->status_name = (new OrderStatus())->getElementById( $order->status)->name;
      switch ($order->status)
      {
        case 1:
        case 2:
        case 3:
      //    $order->status_name_cust = "Создан";
        //  break;
        case 4:
          $order->status_name_cust = "Ожидает доставки";
          break;
        case 5:
          $order->status_name_cust = "Доставлен";
          break;
        case  6:
          $order->status_name_cust = "Отменён";
          break;
      }
      
      
      $order->fillLines();
			return $order;
		}

		public function getLinesSum()
        {
            $sum = 0;
            if ($this->lines != null)
                foreach ($this->lines as $line)
                {
                    $sum += $line->countSum();
                }
            return $sum;
        }
    public function getLinesCount()
        {
            $sum = 0;
            if ($this->lines != null)
                foreach ($this->lines as $line)
                {
                    $sum += $line->quantity*1;
                }

            return $sum;
        }
	
		public function addProduct($productId, $amount = 1, $product_type = "")
		{

		  $line = $this->findLineByProductId($productId,$product_type);
		  if ($line != null)
		    $line->quantity+=$amount;
      else {
        $line = new OrderLine($productId, $this->id, $product_type);
        $line->quantity = $amount;
        array_push($this->lines, $line);
      }
      $this->Save();
		}
		public function removeProduct($productId)
        {
            if ($this->lines == null)
                $this->lines = [];
            //ищем
            $i=0;
            foreach ($this->lines as $line )
            {

                if ( $line->product->id == $productId )
                {//нашли
                    $line->quantity = 0;
                    if (!User::IsCurrUserAuth())
                    {
                       unset($this->lines[$i]);
                    }
                    break;//уходим
                }
                $i++;
            }
            $this->Save();
        }

    public function findLineByProductId($product_id, $product_type = "")
    {

      if ($this->lines == null)
        $this->lines = [];
      //ищем
      $found=null;
      foreach ($this->lines as $line )
      {
        if ( $line->product->id == $product_id && $line->product_type == $product_type )
        {//нашли

          $found = &$line;
          break;//уходим
        }
      }
      return $found;
    }

    public function changeAmountProduct($productId, $quantity, $product_type = "")
    {
      if ($product_type == null) $product_type = "";
      if ($this->lines == null)
        $this->lines = [];
      //ищем
      $found=false;
      foreach ($this->lines as $line )
      {
        if ( $line->product->id == $productId && $line->product_type == $product_type )
        {//нашли
          $line->quantity = $quantity;
          $found = true;
          break;//уходим
        }
      }
      //не нашли - добавляем
      if(!$found) {
        $line = new OrderLine($productId, $this->id, $product_type);
        $line->quantity = $quantity;
        array_push($this->lines, $line);
      }
      $this->Save();
    }

   
    public function mergeCart($oldCart)
    {
      //$this->lines объединить с $oldCart->lines
      if ($oldCart!=null && $oldCart->lines != null)//check if exists and lines
        foreach ($oldCart->lines as $line) {
          $this->addProduct($line->product->id,$line->quantity);
      }

      return $this;
    }


    //найти корзину юзера
    public static function findCartByUser($uid = -1)
    {
        if ($uid == -1)
        {
            if (isset($_SESSION["CurrentCart"])) {
              return unserialize($_SESSION["CurrentCart"]);
            }
            $cart = new Order();
            $cart->type = 1;
            $cart->user_id = $uid;
            $cart->status = 1;
            $cart->Save();
            return $cart;
        }

        $sql = "select * from orders where user_id = '$uid' and type = '1'";
        $db_cart = (new DB())->query($sql)->fetch_assoc();
        if ($db_cart == null)
        {
            $cart = new Order();
            $cart->type = 1;
            $cart->user_id = $uid;
            $cart->status = 1;
            if ($uid>0)
                $cart->insertDB();
        }
        else
        {
            $cart = Order::CreateFromDB($db_cart);
        }
        return $cart;
    }

    public static function GetCurrUserCart()
    {
        return
            self::findCartByUser(User::IsCurrUserAuth()
                ? (User::GetCurrUser()->id)
                : -1
            );
    }
    public function Save()
        {
            if (User::IsCurrUserAuth())
            {
                if ($this->lines != null)
                    foreach ($this->lines as $line)
                        $line->updateDB();
            }
            else {
                $_SESSION ["CurrentCart"] = serialize($this);
            }
        }
    
    public static function getOrdersList($page = 0, $limit = 0)
    {
      $offset = $page * $limit;
      if($limit == 0)
        $sql = "select * from orders where type=2 order by id desc ";
      else $sql = "select * from orders where type=2 order by id desc  limit $offset, $limit";
      
      
      $db = new DB();
      
      $db_prod = $db->query($sql);
      $orders = array();
      while ($ord = $db_prod->fetch_assoc())
      {
        array_push($orders, Order::CreateFromDB($ord));
      }
      return $orders;
    }
        public static function getUserOrdersList($userId, $page = 0, $limit = 0)
        {
            $offset = $page * $limit;
            if($limit == 0)
                $sql = "select * from orders where type=2 and user_id='$userId'";
            else $sql = "select * from orders where type=2 and user_id='$userId' limit $offset, $limit";


            $db = new DB();

            $db_prod = $db->query($sql);
            $orders = array();
            while ($ord = $db_prod->fetch_assoc())
            {
                array_push($orders, Order::CreateFromDB($ord));
            }
            return $orders;
        }

        public static function getUserOrdersSum($userId)
        {
            $orders = Order::getUserOrdersList($userId);

            $sum = 0;

            foreach ($orders as $order) {
                $sum+=$order->getLinesSum();
            }

            return $sum;
        }
         public function getLinesCountPrint()
         {
           $c = $this->getLinesCount();
           $modulus = $c % 10;
  
           if ($modulus == 0)
             return $c . " предметов";
           if ($modulus == 1 && $c != 11)
             return $c . " предмет";
           if ($modulus >= 2 && $modulus <= 4)
             return $c . " предмета";
  
           return $c . " предметов";
  
  
         }
         
  }
	




?>