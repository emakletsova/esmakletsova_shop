<?php
	SESSION_START();
  
  include_once("./Model/common.php");
  
  include_once(SITE_ROOT."Model/user.php");

    $is_authorized = isset($_SESSION["UserId"]);

    if ($is_authorized)
    {
        
        //: если авторизован - го в личный кабинет
		header("location:account.php");
		exit();
    }

?> 
<?php

	$page_title = "Регистрация";
	include("head.php");
	
	
	include("menu.php");
	
	
	
	include("registration.content.php");
	
	
	include("footer.php");
?>