$(document).ready(function()
{
	$('.delete-product').on('click', function()
	{
		
		//Проверить, что имя не пустое и поорать
		if (!confirm("Вы уверены, что хотите удалить?"))
		{
			return;
		}
		
		var pid = $(this).attr("product_id");
		var cid = $(this).attr("category_id");
		
		$.post(
			"/shop/Action/product.delete.php",
			{
				PRODUCT_ID : pid
			},
			function(data)
			{
				//alert(data);
				location.href = "/shop/items_in_category_admin.php?CATEGORY_ID=" + cid;
			},
			"text"
			)
			.fail(function(data)
			{
				alert("Не получилось :(\r\n" + data);
			});
		
	});
	

	$('.save-product').on('click', function()
	{
		
		//Проверить, что имя не пустое и поорать
		if (!confirm("Вы уверены, что хотите сохранить?"))
		{
			return;
		}
		
		var param = {};
		
		param["PRODUCT_ID"] = $(this).attr("product_id");
		
		param["name"] = $('.product-name-admin').val();
		param["description"] = $('.product-description').val();
		

		param["is_hot"] = param["is_new"] = param["is_sale"] = 0;
		
		param["is_hot"] = $('#hotproduct').prop('checked');
		param["is_new"] =$('#newproduct').prop('checked');
		param["is_sale"] = $('#saleproduct').prop('checked');
		
		//.product-type-description
		
		param["product_types"] = []; //new array();
		
		$('.product-type-description').each(function()
		{
			param["product_types"].push($(this).val());
		});
		
		param["media_new"] = [];
		param["media_delete"] = [];
		
		$('.product-image.newphoto').each(function()
		{
			var $media = $(this);
			param["media_new"].push($media.attr('src'));
		});
		$('.product-image.deleteimage').each(function()
		{
			var $media = $(this);
			if (typeof $media.attr('mid') !== "undefined")
				param["media_delete"].push($media.attr('mid'));
		});
		console.log(param);
//return;
		$.post(
			"/shop/Action/product.save.php",
			param,
			function(data)
			{
				console.log(data);
				//location.reload();
			},
			"text"
			)
			.fail(function(data)
			{
				alert("Не получилось :(\r\n" + data);
			});
		
	});
	
});
