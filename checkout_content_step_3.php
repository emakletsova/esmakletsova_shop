<?php

?>


<main class="container shoppingcart">
    <div class="row central-content">
        <div style="height:20px;">
        </div>
    </div>
    <div class="row">
        <div class="col-12 contact-info" style="padding-top: 10px;">
            <span style="color:#ed1651;">1.</span>
            Контактная информация
        </div>
    </div>
    <div class="row" style="border-top: 2px solid #f6f6f6;">
        <div class="col-12 contact-info">
            <span style="color:#ed1651;">2.</span>
            Информация о доставке
        </div>

    </div>

    <div class="row">
        <div class="col-12 information-confirmation">
            <span style="color:#ed1651;">3.</span>
            Подтверждение заказа
        </div>
    </div>

    <div class="row">
        <div class="col-12">

            <div class="row">
                <div class="col-12 form-group" style="margin-top: 33px; margin-left: 30px;">
                    <span class="orderlist">Состав заказа:</span>
                </div>
            </div>
            <div class="row headlineorder">
                <div class="col-6">
                    Товар
                </div>
                <div class="col-2" style="text-align: center;">
                    Стоимость
                </div>
                <div class="col-2" style="text-align: center;">
                    Количество
                </div>
                <div class="col-2" style="text-align: center;">
                    Итого
                </div>
            </div>
            <div class="row lineorder">
            </div>
            <?php
            if ($cart->lines != null)
            foreach ($cart->lines as $line)
            {
            ?>
            <div class="row" style="margin-left: 15px">
                <div class="col-6 nametrade">
                    <?= $line->product->name; ?>
                </div>
                <div class="col-2 price" style="text-align: center;">
                    <?= $line->product->getPrintPrice(); ?>руб.
                </div>
                <div class="col-2 quantitydelivery" style="text-align: center;">
                    <?= $line->quantity; /**/ ?></span>
                </div>
                <div class="col-2 pricetotal" style="margin-top: 40px; text-align: center;">
                    <?= $nombre_format($line->countSum());?>руб.
                </div>
            </div>
            <?php } ?>
            <div class="row">
                <div class="offset-8 col-4 priceontotal">
                    <span class="col-2" style="font-size: 24px; padding-right: 7px; padding-left: 25px;">Итого:</span>
                    <span class="col-2" style=" font-size: 30px;"><?= $nombre_format($cart->getLinesSum()); ?>руб. </span>
                </div>
            </div>
            <div class="row">
                <div class="col-12 form-group" style="margin-top: 33px;">
                    <div style="margin-left: 30px;" class="delivery">Доставка:</div>
                </div>
            </div>
            <div class="row">
                <div class="col-3" style="margin-left: 30px;">
                    <p class="title">Контактное лицо(ФИО):</p>
                    <p class="description"><?= $user->userName ?></p>
                    <p class="title">Контактный телефон:</p>
                    <p class="description"><?= $user->userPhone ?></p>
                    <p class="title">E-mail:</p>
                    <p class="description"><?= $user->email ?></p>
                </div>
                <div class="col-3">
                    <p class="title">Город:</p>
                    <p class="description"><?= $cart->town ?></p>
                    <p class="title">Улица:</p>
                    <p class="description"><?= $cart->street ?></p>
                    <div class="row">
                        <div class="col-6">
                            <p class="title">Дом:</p>
                            <p class="description"><?= $cart->house ?></p>
                        </div>
                        <div class="col-6">
                            <p class="title">Квартира:</p>
                            <p class="description"><?= $cart->apartment ?></p>
                        </div>
                    </div>
                </div>
                <div class="col-3">
                    <p class="title">Способ доставки:</p>
                    <p class="description">Курьерская доставка<br/>  с оплатой при получении</p>
                    <p class="title">Комментарий к заказу:</p>
                    <p class="description"><?= $cart->text_comment ?></p>
                </div>
            </div>

            <div class="row" style="padding-left: 30px; margin-top: 20px;">
                <div class="col-12 form-group">
                    <button type="submit" class="confirmorder" order_id="<?= $cart->id; ?>">Подтвердить заказ</button>
                </div>
            </div>
        </div>
    </div>
</main>
<script src="checkout_content_step_3.script.js" ></script>
