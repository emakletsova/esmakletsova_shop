<?php
 $users = User::getUsersList();
?>


			<div class="col-9 order-list admin-panel">
				<div class="row">
					<div class="col-12 order-list-title">
						ПОЛЬЗОВАТЕЛИ
					</div>
				</div>
				<div class="row user-list-head linecart">
					<div class="col-3 user-list-head-item">
						ИМЯ
					</div>
					<div class="col-3 user-list-head-item" style="text-align: center;">
						E-MAIL
					</div>
					<div class="col-3 user-list-head-item" style="text-align: center;">
						ТЕЛЕФОН
					</div>
					
				</div>
			<?php 
				$us = array();
				for ($i=0; $i<20; $i++)
				{
					if (count($users) == 0)
						break;
					$us[] = array_pop($users);
				}
				foreach ($us as $user)
				{
			?>				
				<div class="row user-list-item">
					<div class="col-3 user-name">
						<?= $user->userName ?>	
					</div>
					<div class="col-3 user-mail">
						<?= $user->email ?>
					</div>
					<div class="col-3 user-phone">
						<?= $user->userPhone ?>
					</div>
					<div class="offset-1 col-2 user-view">
						<a href='user_information_admin.php?UserId=<?= $user->id; ?>' style="border-bottom: 1px solid #e6daec; color: #8e44ad;">просмотр</a>
					</div>
				</div>
				<?php } ?>			
			</div>					
		
			