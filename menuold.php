<?php 
include_once(SITE_ROOT."Model/category.php");
include_once(SITE_ROOT."Model/user.php");
include_once(SITE_ROOT."Model/order.php");

	
	
$allCats = Category::getCategoryList();


$cart = Order::GetCurrUserCart();



?>


<header class="container-fluid menubar">
			
		<div class="container" style="padding: 0 5px 0 0;">
			<div class="row">
				
				<div class="col-2" style="padding-right:10px;">
					<a href="super_shop.php"><img class="logo" src="img/logo.jpg"/></a>
				</div>
				
				<nav class="col-10 white-bar">
					<ul class="row horizontal">
					<?php
						foreach($allCats as $cat)
						{
					?>
						<li class="col-2 horizontal">
							<a href="/shop/category.php?CATEGORY_ID=<?= $cat->id; ?>" ><?= $cat->name; ?></a>
						</li>
					<?php }; ?>						
						<li class="col-3 enter-home horizontal">
						
						<?php if (!User::IsCurrUserAuth()) //не авторизован
							{							?>
							<ul class="row">
								<li class="col-6 enter" style="padding-right: 7px;">
									<img class="icon-enter" src="img/icon-enter.png"/>
									<a href="login.php">Войти</a>
								</li>
								<li class="col-6 registration">
									<a href="registration.php">Регистрация</a>
								</li>
							</ul>
							
						<?php } else { //авторизован 
						?>
							<ul class="row">
								<li class="col-12 enter" style="padding-right: 7px;">
									<img class="icon-enter" src="img/icon-enter.png"/>
									<a href="account.php"><?= User::GetCurrUser()->email ?></a>
								</li>
								<li class="col-12 enter" style="padding-right: 7px;">
									<img class="icon-enter" src="img/icon-enter.png"/>
									<a href="logout.php">Выйти</a>
								</li>
							</ul>
						
						<?php } ?>
						</li>

					</ul>
					<div class="row">
						<a class="offset-9 col-3" href="shopping_cart.php">
							<div class="row">
								<div class="offset-1 col-7 good-backet">					
									<span style="font-size: 20px;">
										<?= $nombre_format($cart->getLinesSum()) ?> руб.
									</span>	
									<br/>
									<span style="font-size: 12px;">
										<?= $cart->getLinesCount() ?> предмета
									</span>
								</div>
								<div class="col-4 good-backet">	
									<img class="cart" src="img/cart-icon.png" />								
								</div>
							</div>
						
						</a>
					</div>
				</nav>
			</div>
				
		</div>
	</header>