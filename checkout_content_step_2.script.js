$('.continue').on('click', function()
{

    if (!confirm("Вы уверены, что хотите сохранить?"))
    {
        return;
    }

    var param = {};

    param["ORDER_ID"] = $(this).attr("order_id");

    param["town"] = $('.your-town').val();
    param["street"] = $('.your-street').val();
    param["house"] = $('.form-address').val();
    param["apartment"] = $('.form-addressapartment').val();
    param["text_comment"] = $('.comment').val();

    param["type_delivery"] = $('input[name="delivery"]:checked').val();

    console.log(param);
    $.post(
            "/shop/Action/order.step_2.php",
            param,
            function(data)
            {
                console.log(data);
                location.href = "checkout.php?STEP=3";
            },
            "text"
        )
        .fail(function(data)
        {
            alert("Не получилось :(\r\n" + data);
        });
    event.preventDefault()
    return false;
});

