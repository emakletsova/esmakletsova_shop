<?php 
include_once(SITE_ROOT."Model/category.php");
include_once(SITE_ROOT."Model/user.php");
include_once(SITE_ROOT."Model/order.php");
	
	
$allCats = Category::getCategoryList();
$cart = Order::GetCurrUserCart();


?>


<header class="container-fluid menubar">
			
		<div class="container" style="padding: 0 5px 0 0;">
			<div class="row">
				
				<div class="col-2" style="padding-right:10px;">
					<a href="super_shop.php"><img class="logo" src="img/logo.jpg"/></a>
				</div>

                <nav class="col-10 white-bar">
                    <ul class="clearfix horizontal">
                        <li class=" horizontal qwe" style="width: 100px;float:left;line-height: 50px;">
                            <a href="/shop/category.php?CATEGORY_ID=61">Вейкборды</a>
                        </li>
                        <li class=" horizontal qwe" style="width:120px;float:left;line-height: 27px;">
                            <a href="/shop/category.php?CATEGORY_ID=62">Двухколесные скейты</a>
                        </li>
                        <li class=" horizontal qwe" style="width:120px;float:left;line-height: 27px;">
                            <a href="/shop/category.php?CATEGORY_ID=63">Роликовые коньки</a>
                        </li>
                        <li class=" horizontal qwe" style="width: 90px;float:left;line-height: 50px;">
                            <a href="/shop/category.php?CATEGORY_ID=64">Самокаты</a>
                        </li>
                        <li class=" horizontal qwe" style="width:120px; float:left;line-height: 50px;">
                            <a href="/shop/category.php?CATEGORY_ID=65">Сноуборды</a>
                        </li>
                        <li class=" horizontal qwe" style="width:120px;float:left;line-height: 27px;">
                            <a href="/shop/category.php?CATEGORY_ID=66">Теннисные ракетки</a>
                        </li>

                        <li class=" enter-home horizontal"
                            <?php if (!User::IsCurrUserAuth()) //не авторизован
                          {							?>

                              style="padding-right: 7px; line-height: 50px; width: 200px; padding-left: 10px; float: left;">
                              <img class="icon-enter" src="img/icon-enter.png"/>
                              <a href="login.php" style="b">Войти</a>&nbsp;&nbsp;&nbsp;&nbsp;
                              <a href="registration.php">Регистрация</a>
  
                          <?php } else { //авторизован
                            ?>
                              style="padding-right: 7px; line-height: 27px; width: 200px; padding-left: 10px; float: left;">

                              <img class="icon-enter" src="img/icon-enter.png"/>
                                <a href="account.php" style="font-family:Supermolot Light; font-size: 14px; "><?= User::GetCurrUser()->email ?></a>
                                <br/>
                                <img class="icon-out" src="img/icon-out.png"/>
                                <a href="logout.php" style="font-family:Supermolot Light; font-size: 14px;">Выйти</a>
                          <?php } ?>
                        </li>

                    </ul>
                    <div class="row">
                        <a class="offset-9 col-3"  href="shopping_cart.php">
                            <div class="row">
                                <div class="offset-1 col-7 good-backet">
									<span style="font-size: 20px;">
										<?= $nombre_format($cart->getLinesSum()); ?> руб.
									</span>
                                    <br/>
                                    <span style="font-size: 12px;">
										<?= $cart->getLinesCountPrint(); ?>
									</span>
                                </div>
                                <div class="col-4 good-backet">
                                    <img class="cart" src="img/cart-icon.png" />
                                </div>
                            </div>

                        </a>
                    </div>
                </nav>
                
                
			</div>
				
		</div>
	</header>