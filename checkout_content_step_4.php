<?php
?>


<main class="container ordercart">


    <div class="row">
        <div class="col-12 ordergood">
            <span class="ordernumber">Заказ № <?= $cart->id ?></span> <span class="successfullyissued">успешно оформлен</span>
        </div>
    </div>
    <div class="row">
        <div class="col-12 thankyou">
            Спасибо за ваш заказ.
        </div>
    </div>
    <div class="row">
        <div class="col-12 timeorder">
            В ближайшее время с вами свяжется оператор <br/> для уточнения времени доставки.
        </div>
    </div>

    <div class="col-12 form-group" style="margin-top: 20px;">
        <button type="submit" class="backstore"><a style="color:#fff; text-decoration: none;" href="super_shop.php">Вернуться в магазин</a></button>
    </div>

</main>
