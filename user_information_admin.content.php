<?php

?>
        <div class="col-9 order-list admin-panel">
			<div class="row">
					<div class="col-12 order-list-title">
						ПРОСМОТР ПОЛЬЗОВАТЕЛЯ
					</div>
				</div>
			<div class="row order-list-head linecart">
					<div class="col-12 order-list-head-item">
							ИНФОРМАЦИЯ О ПОЛЬЗОВАТЕЛЕ
					</div>
				</div>	
				<div class="row order-list-information" style="padding-top: 20px;">
					<div class="col-4">
						<div class="row">
							<div class="col-12">
							    <span class="title-information">Контактное лицо(ФИО):</span>
								<input class="description" type="text" id="fio" name="fio" value="<?= $user->userName ?>"/>
							</div>					
							<div class="col-12">
								<span class="title-information">Контактный телефон:</span>
								<input class="description" type="text" id="number" name="number" value="<?= $user->userPhone ?>"/>
							</div>									
							<div class="col-12">
								<span class="title-information">E-mail:</span>
								<input class="description" type="text" id="email" name="email" value="<?= $user->email ?>"/>
							</div>	
						</div>	
					</div>
					<div class="col-4">
						<div class="row">
							<div class="col-12">
								<span class="title-information">Город:</span>
								<input class="description" type="text" d="town" name="town" value="<?= $user->userTown ?>"/>
							</div>
							<div class="col-12">
								<span class="title-information">Улица:</span>
								<input class="description" type="text" id="street" name="street" value="<?= $user->userStreet ?>"/>
							</div>
						</div>
							
						<div class="row">
							<div class="col-4">
								<span class="title-information">Дом:</span>
								<input class="description" type="text" id="house" name="house" value="<?= $user->userHouse ?>"/>
							</div>
							<div class="col-4">
								<span class="title-information">Квартира:</span>
								<input class="description" type="text" id="apartment" name="apartment" value="<?= $user->userApartment ?>"/>
							</div>
						</div>
					</div>							
				</div>
				<div class="row empty-line">
				</div>
				<div class="row order-list-head linecart">
					<div class="col-12 order-list-head-item">
							ИСТОРИЯ ЗАКАЗОВ
					</div>
					
				</div>
                <?php
                $orders = Order::getUserOrdersList($_GET["UserId"]);
                foreach ($orders as $order) {

                ?>
				<div class="row history-list">
					<div class="col-4 order-number">
							<a class="order-number" href="/shop/order_information_admin.php?OrderId=<?= $order->id ?>">
							№<?= $order->id ?>
						</a>
					</div>
					<div class="col-4 full-order-price">
                        <?= $nombre_format($order->getLinesSum()); ?> руб.
					</div>
					<div class="offset-1 col-3 time-order">
                        <?= date("d.m.Y в H:i:s", strtotime( $order->confirm_time ) ) ?>
					</div>
				</div>
                <?php } ?>

				<div class="row order-list-total-amount">
                    <div class="col-12" style="text-align: right; white-space: nowrap;">
                        <span class="amount-price-text"
                              style="padding-top: 14px; line-height: 16px; display: inline-block; text-transform: uppercase;">
                                ИТОГОВАЯ СУММА<br/> заказов
                        </span>&nbsp;
						<span class="amount-price">
                          <?=$nombre_format(Order::getUserOrdersSum($user->id)); ?><span style="font-family: Proxima Nova Light; font-size: 22px; ">руб.</span>
                        </span>
					</div>

				</div>

				<div class="row">
					<div class="offset-8 col-4" style="text-align: right; padding-right: 0;">
					<span class="delete-user" uid="<?=$user->id;?>" style="cursor:pointer;">
							Удалить пользователя
						</span>
					</div>
				</div>
			</div>


<script>
    $(document).ready(function()
    {
        $('.delete-user').on('click', function()
        {

            //Проверить, что имя не пустое и поорать
            if (!confirm("Вы уверены, что хотите удалить?"))
            {
                return;
            }
            var cid = $(this).attr("uid");
            $.post(
                "/shop/Action/user.delete.php",
                {
                    "USER_ID" : cid
                },
                function(data)
                {
                    //alert(data);
                    location.href="/shop/user_admin.php";
                },
                "text"
            )
                .fail(function(data)
                {
                    alert("Не получилось :(\r\n" + data);
                });

        });

    });
    
</script>