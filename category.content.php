<?php
	
	$products = $category->getProductList($PAGE, $PRODUCTS_PER_PAGE);
	$total_products = $category->getProductCount();
	$products_from = 1+$PAGE*$PRODUCTS_PER_PAGE;
	$products_to = (1+$PAGE)*$PRODUCTS_PER_PAGE;
	if ($products_to > $total_products)
		$products_to = $total_products;
	
	$TOTAL_PAGES = $total_products / $PRODUCTS_PER_PAGE;
  $promo = Product::getPromoProduct();
  $promo2 = array_pop($promo);
  $promo1 = array_pop($promo);
  $promo3 = array_pop($promo);
  $promo4 = array_pop($promo);
  
  $banner_need = $PAGE==0;
  $banner_style = $banner_need ? "" : "display:none;";
  
?>


	<main class="container main-block">
		<div class="row">
			<div class="col-12 category-title">
				<?= $category->name; ?>
			</div>
		 
		
			<div class="col-12 product-count">
			Показано 
				<?= $products_from; ?> 
				– 
				<?= $products_to; ?> 
				из <?= $total_products; ?> товаров
			
		    </div>
        </div>
		<div class="row pager" style="margin-top:36px;">
			<div class="offset-10 col-2">
				<span style="font-family:Supermolot Light;font-size: 14px; color:#333333;">
					страницы 
					<?php
						for($i=0; $i<$TOTAL_PAGES; $i++)
						{
							echo "<a class='paginator ". ($i==$PAGE ? "current" : "") ."' href='category.php?CATEGORY_ID=$category->id&PAGE=$i'>". ($i + 1) . "</a>";
						}
					?>
				</span>
			</div>
		</div>  
			
		<div class="row" style="margin-top:36px;">
		
			<div class="col-9 banner-kat" style="<?= $banner_style ?>">
              
                     <div class="col-10 category-description"><?= $category->catDescription; ?></div>
                    <div class="col-6 shot-description"><?= $category->shotDescription; ?></div>
                
			</div>
		
			<?php 
				$prod = array();
				for ($i=0; $i<9; $i++)
				{
					if (count($products) == 0)
						break;
					$prod[] = array_pop($products);
				}
				foreach ($prod as $p)
				{
			?>
			<a class="col-3 product-item comebacktoproduct" href='/shop/product.php?PRODUCT_ID=<?= $p->id; ?>';>
				<div class="row">
					<div class="col-12 product-image-box">
						<img src="<?= $p->getMedias()[0]->path; ?>" class="product-image" />
                        <?= $p->getFlag(); ?>
					</div>
				</div>
				<div class="row">
					<div class="col-6 product-name">
						<?= $p->name; ?>
					</div>
					<div class="col-6 product-price">
						<?= $p->getPrintPrice(); ?> руб.
					</div>						
				</div>
			
			</a>
				<?php } ?>
		
        <?php if ($banner_need) { ?>
        </div>
		
		<div class="row">
			<div class="col-6">
				<div class="row">
				<?php } ?>
				<?php
				unset($prod);
				$prod = array();
				for ($i=0; $i<4; $i++)
				{
					if (count($products) == 0)
						break;
					$prod[] = array_pop($products);
				}
				
				foreach ($prod as $p)
				{
				?>
					<a class=" <?= $banner_need?"col-6":"col-3"?> product-item comebacktoproduct" href='/shop/product.php?PRODUCT_ID=<?= $p->id; ?>';>
						<div class="row">
							<div class="col-12 product-image-box">
								<img src="<?= $p->getMedias()[0]->path; ?>" class="product-image" />
                                <?= $p->getFlag(); ?>
							</div>
						
							<div class="col-6 product-name">
								<?= $p->name; ?>
							</div>
							<div class="col-6 product-price">
                                <?= $p->getPrintPrice(); ?> руб.
							</div>
                        </div>
							
					</a>
					<?php } ?>
                    
                    <?php if ( $banner_need) { ?>
				</div>	
			</div>
			<div class="col-6 banner-small-4 img-responsive"  style="<?= $banner_style ?>">
                <div class="row" style="margin-top: 50px; margin-left: 30px; ">
                    <div class="col-12" style="font-size:36px; font-family: Supermolot Bold Italic;">
						    <?= $promo1->name; ?>
					</div>
                    <div class="col-6" style="font-size:14px; font-family: Supermolot Light;">
                            <?= $promo1->description; ?>
					</div>
                    <div class="col-12" style="font-size:36px; font-family: Supermolot Bold Italic; color: #ed1651; margin-top: 20px;">
						    <?= $promo1->getPrintPrice(); ?> руб.
					</div>
                
               
                    <div class="col-5">
                        <a class="lookatthis" href='/shop/product.php?PRODUCT_ID=<?= $promo1->id; ?>';>
                            Посмотреть +
                        </a>
                    </div>
                </div>
				
			</div>
		</div>
		
		<div class="row">
		
			<?php } ?>
			<?php 
				unset($prod);
				$prod = array();
				for ($i=0; $i<8; $i++)
				{
					if (count($products) == 0)
						break;
					$prod[] = array_pop($products);
				}
				foreach ($prod as $p)
				{
			?>
			<a class="col-3 product-item comebacktoproduct" href='/shop/product.php?PRODUCT_ID=<?= $p->id; ?>';>
				<div class="row">
					<div class="col-12 product-image-box">
						<img src="<?= $p->getMedias()[0]->path; ?>" class="product-image" />
                        <?= $p->getFlag(); ?>
					</div>
				</div>
				<div class="row">
					<div class="col-6 product-name">
						<?= $p->name; ?>
					</div>
					<div class="col-6 product-price">
                        <?= $p->getPrintPrice(); ?> руб.
					</div>						
				</div>
			
			</a>
				<?php } ?>
			
		</div>



        <div class="row pager" style="margin-top:36px;">
            <div class="offset-10 col-2">
				<span style="font-family:Supermolot Light;font-size: 14px; color:#333333;">
					страницы
					<?php
                      for($i=0; $i<$TOTAL_PAGES; $i++)
                      {
                        echo "<a class='paginator ". ($i==$PAGE ? "current" : "") ."' href='category.php?CATEGORY_ID=$category->id&PAGE=$i'>". ($i + 1) . "</a>";
                      }
                    ?>
				</span>
            </div>
        </div>
    </main>