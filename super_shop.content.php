<?php
    $new_products = Product::getNewProduct();
    $pop_products = Product::getPopularProduct();
    $promo = Product::getPromoProduct();
    $promo2 = array_pop($promo);
    $promo1 = array_pop($promo);
    $promo3 = array_pop($promo);
    $promo4 = array_pop($promo);
 
?>
<link rel="stylesheet" href="https://unpkg.com/swiper/css/swiper.min.css">

<script src="https://unpkg.com/swiper/js/swiper.min.js"></script>
<body class="central-content">
	
	<header class="container-fluid banner-glavnay" style="height: 700px;">
				
			<div class="row" style="margin-top: 40px; margin-left: 22px; ">
				<div class="col-5" style="margin-left: 125px;">
					<span style="font-size:72px; font-family: Supermolot LightItalic;">
						<?= $promo1->name; ?>
					</span><br/>
					<span style="font-size:16px; color:white; font-family: Supermolot Regular;">
						<?= $promo1->description; ?>
					</span>
				</div>
			</div>
			
			<div class="row">
				<div class="offset-5 col-2">
					<a class="lookat" href='/shop/product.php?PRODUCT_ID=<?= $promo1->id; ?>';>
						Посмотреть +
					</a>
				</div>
			</div>
   

	</header>
	
	
	<main class="container allproduct">
		<div class="row newtrade">
			<div class="col-11">
				<span>
							Новые товары
				</span>
			</div>
            <div class="col-1">
                <span class="swiper-button-prev-3"></span> <span class="swiper-button-next-3"></span>
            </div>
		</div>  
			
		<div class="row newproduct">
            <div class="swiper-container swiper-wrapper-3" style="height: 770px; background-color: white;">
                <div class="swiper-wrapper" style="height: 400px">
		
		<?php 
				foreach ($new_products as $p)
				{
			?>
			<a class="colq-3 product-item comebacktoproduct swiper-slide" href='/shop/product.php?PRODUCT_ID=<?= $p->id; ?>';>
				<div class="row" style="background:#fff;">
					<div class="col-12 product-image-box slider-preview">
						<img src="<?= $p->getMedias()[0]->path; ?>" class="product-image" />
                        <?= $p->getFlag(); ?>
					</div>
					<div class="col-6 product-name">
						<?= $p->name; ?>
					</div>
					<div class="col-6 product-price">
						<?= $p->getPrintPrice(); ?> руб.
					</div>
				</div>
			
			</a>
				<?php } ?>
		        </div>
            </div>
        </div>

		<div class="row central-content">
			<div style="height:20px;">
			</div>
		</div>	
		<div class="row banner">
			<a class="col-3 banner-1" href='/shop/product.php?PRODUCT_ID=<?= $promo2->id; ?>'>
				<span class="headline-trade" >
					<?= $promo2->name; ?>
				</span>
			</a>
			<a class="col-3 banner-2" href='/shop/product.php?PRODUCT_ID=<?= $promo3->id; ?>'>
				<span class="headline-trade">
					<?= $promo3->name; ?>
			</span>
			</a>
			<a class="col-6 banner-3" href='/shop/product.php?PRODUCT_ID=<?= $promo4->id; ?>'>
				<span class="col-6 headline-trade" style="color:white;">
					<?= $promo4->name; ?>
				
				</span>
			</a>
		</div>

		<div class="row central-content">
			<div style="height:90px;">
			</div>
		</div>	
		
		<div class="row allproduct populartrade">
			<div class="col-11">
				<span>
							Популярные товары
				</span>
			</div>
			<div class="col-1">
                <span class="swiper-button-prev-2"></span> <span class="swiper-button-next-2"></span>
			</div>
		</div>  
									
		<div class="row popularproduct">
            <div class="swiper-container swiper-wrapper-2" style="height: 350px;">
                <div class="swiper-wrapper">
		
		<?php 
				$prod = array();
				for ($i=0; $i<4; $i++)
				{
					if (count($pop_products) == 0)
						break;
					$prod[] = array_pop($pop_products);
				}
				foreach ($prod as $p)
				{
			?>
			<a class="col-3 product-item comebacktoproduct swiper-slide"  href='/shop/product.php?PRODUCT_ID=<?= $p->id; ?>';>
				<div class="row">
					<div class="col-12 product-image-box slider-preview">
						<img src="<?= $p->getMedias()[0]->path; ?>" class="product-image" />
                        <?= $p->getFlag(); ?>
					</div>

					<div class="col-6 product-name">
						<?= $p->name; ?>
					</div>
					<div class="col-6 product-price">
                        <?= $p->getPrintPrice(); ?> руб.
					</div>						
				</div>
			
			</a>
				<?php } ?>
                </div>
            </div>
		</div>
			
		<div class="row central-content">
			<div style="height:20px;">
			</div>
		</div>		
	</main>
    <div class="container banner-footer">
        <div class="row">
            <div class="offset-5 col-7" style="margin-top: 42px; font-weight:bold;">

                <span style="font-size: 24px; color:#021562; font-family: Supermolot Italic; margin-top: 42px;">
					О магазине
				</span>
            </div>
            <div class="offset-5 col-7" style="margin-top: 20px;">
			<span style="font-size: 14px; font-family: Supermolot Light;">


				<p>
				Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo<br/> ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis<br/> parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec,<br/> pellentesque eu, pretium quis, sem. </p>

				<p>
				Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec,<br/> vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae,<br/> justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus.
				</p>
				</span>
            </div>
        </div>
    </div>

    <script>

        <!-- Initialize Swiper -->
        var qqq = new Swiper('.swiper-wrapper-3', {
            slidesPerView: 4,
            slidesPerColumn: 2,
            slidesPerColumnFill:'row',
            navigation: {
                nextEl: '.swiper-button-next-3',
                prevEl: '.swiper-button-prev-3',
            }

        });
        var qwe = new Swiper('.swiper-wrapper-2', {
            spaceBetween: 0,
            loop: true,
            loopedSlides: 4 ,
            slidesPerView: 4,
            navigation: {
                nextEl: '.swiper-button-next-2',
                prevEl: '.swiper-button-prev-2',
            }

        });

    </script>
