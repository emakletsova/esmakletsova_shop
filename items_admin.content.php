<?php

	$db_cats = Category::getAmountProduct();
	
	
?>

			<div class="col-9 order-list admin-panel">
				<div class="row">
					<div class="col-12 order-list-title">
						КАТЕГОРИИ
					</div>
				</div>
				<div class="row items-list-head linecart">
					<div class="col-4 items-list-head-item">
						НАЗВАНИЕ КАТЕГОРИИ
					</div>
					<div class="col-4 items-list-head-item" style="text-align: center;">
						КОЛИЧЕСТВО ТОВАРОВ
					</div>
					
				</div>			
				
				<?php 
					while ($cat = $db_cats->fetch_assoc())
					{	?>
				<div class="row items-list-item">
					<div class="col-4 product-name">
						<img src="img/img-case.png"/>
						<?= $cat['name']; ?>
					</div>
					<div class="col-4 product-amount">
						<?= $cat['amount']; ?>
					</div>
					<div class="col-2 product-view" 
							style="color:red; <?= $cat['amount']==0 ? "": "display:none;" ?>">
						<span  class="product-delete"
						style="border-bottom: 1px solid #8e44ad;cursor:pointer;"
						category_id="<?= $cat['category_id']; ?>">Удалить</span>
					</div>
					<div class="<?= $cat['amount']==0 ? "": "offset-2" ?> col-2 product-view">
						<a href="/shop/items_in_category_admin.php?CATEGORY_ID=<?= $cat['category_id']; ?>" style="border-bottom: 1px solid #e6daec; color: #8e44ad;">просмотр</a>
					</div>
				</div>
				
				<?php } ?>
			
				<div class="row">
					<div class="offset-4 col-8" style="margin-top: 20px;padding-right: 0;text-align: right">
						<span class="title-kat">Добавить категорию:</span>
						<input class="adding-new-cat" type="text"
							value="<?= $category->name ?>" 
							category_id="<?= $category->id ?>"/>
					</div>
					<div class="offset-9 col-3" style="text-align: right; padding-right: 0;">
						<span class="add-new-category" style="cursor:pointer;">добавить категорию</span>
				    </div>
				</div>
			</div>			
				
		<script>
			<?php include("items_admin.script.js"); ?>
		</script>