<?php

	$products = $category==null ? Category::getAllProductList() : $category->getProductList();

?>

			<div class="col-9 order-list admin-panel">
				<div class="row">
					<div class="col-12 order-list-title">
						ТОВАРЫ
					</div>
				</div>
                <?php if ($category != null) { ?>
				<div class="row" style="margin-left: -12px;">
					<div class="col-9">
						<span class="title-kat">Текущая категория:</span>
						<input class="description-cat" type="text"
							value="<?= $category->name ?>" 
							category_id="<?= $category->id ?>"
						/>
						<span class="change-name-kat" style="cursor:pointer;">переименовать</span>
					</div>	
				</div>
                <?php } ?>
				<div class="row items-list-head linecart">
					<div class="col-4 product-list-head-item">
						НАЗВАНИЕ ТОВАРА
					</div>
					<div class="col-4 items-list-head-item" style="text-align: center;">
						СТОИМОСТЬ
					</div>
					
				</div>	
				<?php 
				
				foreach ($products as $p)
				{
				?>				
				<div class="row order-list-item">
					<div class="col-4 product-name">
						<?= $p->name ?>
					</div>
					<div class="col-4 product-price">
					<?= $p->getPrintPrice(); ?>
					</div>
					<div class="offset-2 col-2 product-view">
						<a href="/shop/item_information_admin.php?PRODUCT_ID=<?= $p->id ?>"
                           style="border-bottom: 1px solid #e6daec; color: #8e44ad;" >просмотр</a>
					</div>
				</div>
				
				<?php } ?>
						
			</div>			

			<script>
				<?php include("items_in_category_admin.script.js"); ?>
			</script>
			
		