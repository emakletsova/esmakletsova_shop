<?php
	SESSION_START();
  
  include_once("./Model/common.php");
  
  include_once(SITE_ROOT."Model/order.php");
    include_once(SITE_ROOT."Model/user.php");

    if (!User::IsCurrUserAuth())
    {
        
        //: если не авторизован - го в логин
		header("location:login.php");
		exit();
    }
	
	$user = User::GetCurrUser();
	//var_dump($user);
?> 
<?php

	$page_title = "Личный кабинет";
	include("head.php");
	
	
	include("menu.php");
	
	include("account.content.php");
	
	include("footer.php");
?>