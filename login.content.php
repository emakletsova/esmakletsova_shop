﻿	
	<main class="container registrationperson">
	
		<div class="row">
			<div class="col-12 login-title">
				ВХОД
			</div>
		</div> 		 
			
		<div class="row">
		  
			<div class="col-6">
				<form action="login.action.php" method="GET">
					<div class="col-12 form-group" style="margin-top: 32px;">
						<span class="regperson">Зарегистрированный пользователь</span>
					</div>
					<div class="col-12 form-group">
						<label for="mail">E-mail адрес:</label>
						<input type="email" class="form-reg-new-person"  id="email" name="email" placeholder="mail@company.ru"/>
					</div>
					<div class="col-12 form-group" style="margin-top:30px;">
						<label for="pws">Пароль:</label>
						<input type="password" class="form-reg-new-person" id="pws" name="pws" style="font-family: Supermolot Bold; font-size: 16px; type="password"/>
					</div>
					<div class="col-12 form-group">
						<button type="submit" class="enterperson">Войти</button>
						<span style="padding-left: 20px;"> 
							<a class="forgetpws" href="recover.php">Забыли пароль?</a>
						</span>
					</div>
				</form>      <?= @$_GET["error"] ?>
			</div>
			<div class="col-6 newreg">
				
					<div class="col-12 form-group" style="margin-top: 32px;">
						<span class="regperson">Новый пользователь</span>
					</div>
					<div class="col-12 form-group">
						<a href="registration.php"><button type="submit" class="regnewperson">Зарегистрироваться</button></a>
					</div>
              
     
			</div>
		</div>

    </main>
	
	<script>
		
		function validateEmail(email) {
			var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
			return re.test(String(email).toLowerCase());
		}		
		
		function validate()
		{					
			var email = document.getElementById('email');
			if (!validateEmail(email.value))
			{
				email.focus();
				alert('Проверьте email!');
				return false;
			} 
			if (!confirm('Сохранить?'))
				return false;

				
			return true;
		}
	</script>