<?php
SESSION_START();
  
  include_once("./Model/common.php");
  
  include_once(SITE_ROOT."Model/user.php");
  include_once(SITE_ROOT."Model/order.php");
  include_once(SITE_ROOT."Model/DeliveryType.php");


$user = User::IsCurrUserAuth() ? User::GetCurrUser() : null;
$cart = Order::GetCurrUserCart();

?>

<?php
$page_title = "Оформление заказа";
include("head.php");


include("menu.php");

include("checkout_content.php");

include("footer.php");
?>