	
	<footer class="container">
		
		<div class="row central-content">
			<div style="height:20px;">
			</div>
		</div>
		<div class="row" style="background-color:#01104c; height: 56px;">
			
					<div class="col-10 pattern">
						
							Шаблон для экзаменационного задания. <br/> 
							Разработан специально для «Всероссийской Школы Программирования» <br/> 
							http://bedev.ru/
					</div>
					<div class="offset-1 col-1 up">
						
							<span id="upstairs" style="cursor:pointer;">Наверх &#9650;</span>
					</div>
		
		</div>	
	</footer>
	
	
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
	<script >
	<?php include("footer.button.up.js"); ?>
	</script>
</body>
</html>