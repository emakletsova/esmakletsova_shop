<?php
  SESSION_START();
  
  include_once("./../Model/common.php");
  
  
  
  include_once(SITE_ROOT."Model/order.php");
  include_once(SITE_ROOT."Model/user.php");
  
  $order_id = $_POST["order_id"] ?: null;
  $product_id = $_POST["product_id"];
  
  if ($order_id == null)
    $cart = Order::GetCurrUserCart();
  else $cart = new Order($order_id);
  
  $cart->removeProduct($product_id);

  exit();

?>