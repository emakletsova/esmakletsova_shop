 <?php
   
   
   function create_guid(){
     if (function_exists('com_create_guid')){
       return com_create_guid();
     }
     else {
       mt_srand((double)microtime()*10000);//optional for php 4.2.0 and up.
       $charid = strtoupper(md5(uniqid(rand(), true)));
       $hyphen = chr(45);// "-"
       $uuid = chr(123)// "{"
           .substr($charid, 0, 8).$hyphen
           .substr($charid, 8, 4).$hyphen
           .substr($charid,12, 4).$hyphen
           .substr($charid,16, 4).$hyphen
           .substr($charid,20,12)
           .chr(125);// "}"
       return $uuid;
     }
   }
   
   include_once("./../Model/common.php");
  
   include_once(SITE_ROOT."Model/product.php");
	
	$PRODUCT_ID = $_POST["PRODUCT_ID"];
	
	
	//var_dump($_POST);
	//exit();
	
	
	$product = new Product($PRODUCT_ID);
		
	$product->name = $_POST["name"];
	$product->description = $_POST["description"];
	// $product->popularProduct = (is_hot == true) ? 1 : 0;
	//равносиль
	/*
	if ($POST["is_hot"])
		$product->popularProduct = 1;
	else $product->popularProduct = 0;
	*/
	$product->newProduct = $_POST["is_new"] == 'true' ? 1 : 0;
	$product->saleProduct = $_POST["is_sale"] == 'true' ? 1 : 0;
	$product->popularProduct = $_POST["is_hot"] == 'true' ? 1 : 0;
	
	if (!is_array($_POST["product_types"])) $_POST["product_types"] = [];
	if (count(array_diff($product->types,$_POST["product_types"])) != 0)
	{
		$product->types = $_POST["product_types"];
		$product->clearTypes();
	}		
	
	
	$product->updateDB();
	
	
	//: delete|save media
	/*
	//Получить список файлов в папке
	//Проверить наличие картинки в папке
	//Удалить из базы данных
	//Удалить картинку из папки
	*/
	/*
	с каждым идешником:
	ищем картинку в базе
	отвязываем от продукта
	*если сирота - убиваем (из базы, потом с диска) :(
	*/
	
	//["media_delete"] - > ['id1','id2'] отцепить файл от продукта экземпляр медиа
	if (isset($_POST["media_delete"]))
        foreach ($_POST["media_delete"] as $mid)
        {
            $media = new Media($mid);
            $product->unlinkMediaFromProduct($media);
        }
	
	
	//["media_new"] -> ['file','fil2'] путь как в массовой загрузки
	/*	
	с каждым файлом: добавить к товару
		-создать запись о файле в базе
			--new Media() и заполнить поля
			--сохранить ->updateDB();
		-положить файл в папку загузок "./files/"
			--$media->writeFile($file);
		-привязать к товару в базе
			--$product->addMediaToProduct($media)
	*/
 if (isset($_POST["media_new"]))
     foreach(@$_POST["media_new"] as $file)
     {
        // https://stackoverflow.com/questions/11511511

        if (preg_match('/^data:image\/(\w+);base64,/', $file, $type)) {
            $file = substr($file, strpos($file, ',') + 1);
            $type = strtolower($type[1]); // jpg, png, gif

            if (!in_array($type, [ 'jpg', 'jpeg', 'gif', 'png' ])) {
                throw new \Exception('Неверный тип файла');
            }

            $file = base64_decode($file);

            if ($file === false) {
                throw new \Exception('base64_decode не смог раскодировать файл');
            }
        } else {
            throw new \Exception('неверные данные');
        }
        //
		$media = new Media();
		$media->path = '../files/'.create_guid().'.'.$type;//guid - уникальный ид, во избежание совпадений
		$media->updateDB();
		$media->writeFile($file);
		$product->addMediaToProduct($media);
	}
	
	
	exit();

?>