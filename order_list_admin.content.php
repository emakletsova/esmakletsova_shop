<?php
    $orders = Order::getOrdersList();
?>

<style>
    .status-select-option-1
    {
        color: #0a8eaf;
    }
    .status-select-option-2
    {
        color:  #1ba254;
    }
    .status-select-option-3
    {
        color: #ad5a00;
    }
    .status-select-option-4
    {
        color: #a01ba2;
    }
    .status-select-option-5
    {
        color: #6c6c6c;
    }
    
</style>

<div class="col-9 order-list admin-panel">
  <div class="row">
    <div class="col-12 order-list-title">
      ЗАКАЗЫ
    </div>
  </div>
  <div class="row order-list-head linecart">
    <div class="col-3 order-list-head-item">
      НОМЕР ЗАКАЗА
    </div>
    <div class="col-2 order-list-head-item">
      СТАТУС
    </div>
    <div class="col-2 order-list-head-item">
      СУММА
    </div>
    <div class="col-3 order-list-head-item">
      ВРЕМЯ ЗАКАЗА
    </div>
    <div class="col-2 order-list-head-item">
    
    </div>
  
  </div>
  <?php foreach ($orders as $order) {
  
  ?>
  <div class="row order-list-item">
    <div class="col-3 order-info">
						<span class="order-number">
							№<?= $order->id ?>
						</span>
      <span class="ot">
							от
						</span>
      <span class="user-info">
							<?= (new User($order->user_id))->email ?>
						</span>
    </div>
    
    <div class="col-2 order-status">
            <select class="order-status-select" oid="<?= $order->id ?>">
                <?php
                  $statusList = (new OrderStatus())->getElements();
                  foreach ($statusList as $status) {
                    ?>
                      <option
                                class="status-select-option-<?= $status->id ?>"
                                <?= $order->status == $status->id ? "selected" : "" ?>
                                value="<?= $status->id ?>">
                        <?= $status->name ?>
                      </option>
                <?php
                    
                    }
                ?>
                
                
            </select>
      <span style="display: none;"><?= $order->status_name ?></span>
    </div>
    <div class="col-2 order-price">
      <?= $nombre_format($order->getLinesSum()); ?> руб.
    </div>
    <div class="col-3 order-datetime">
      <?= date("d.m.Y в H:i:s", strtotime( $order->confirm_time ) ) ?>
    </div>
    <div class="col-2 order-view">
      <a href="order_information_admin.php?OrderId=<?= $order->id ?>" style="border-bottom: 1px solid #e6daec; color: #8e44ad;">просмотр</a>
    </div>
  </div>

  <?php } ?>

</div>

<script>
    
    //order-status-select
    $('.order-status-select').on('change', function (e) {

        var $status = $(this).children("option:selected");
        if (!confirm( "Сменить статус на " + $status.text().trim() + "?"  )) {
            return false;
        }
        $(this).css("color", $status.css("color"));
        var param = {};
        param["ORDER_ID"] = $(this).attr("oid");
        param["STATUS_ID"] = $status.val();
        $.post(
            "/shop/Action/order.setStatus.php",
            param,
            function (data) {
                console.log(data);
               // location.reload();
            },
            "text"
        )
            .fail(function (data) {
                alert("Не получилось :(\r\n" + data);
            });

    })
        .each(function () {
            $(this).css("color", $(this).children("option:selected").css("color"));
        });
    
</script>
